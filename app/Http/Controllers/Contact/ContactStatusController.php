<?php

namespace App\Http\Controllers\Contact;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Repositories\ContactRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class ContactStatusController.
 */
class ContactStatusController extends BaseController
{

    /**
     * @var ContactRepository
     */
    private $repository;

    /**
     * ContactStatusController constructor.
     *
     * @param  ContactRepository  $repository
     */
    public function __construct(ContactRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Contact Management';
        $this->module_sub = 'Contact';
        $this->module_name = 'contact';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-contact';
        $this->main_heading = 'Contact Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     *
     * @return mixed
     */
    public function getDeactivated()
    {
        return view($this->module_view.'.deactivated')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Deactivated '.Str::plural($this->module_sub));
    }

    /**
     *
     * @return mixed
     */
    public function getDeleted()
    {
        return view($this->module_view.'.trashed')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Trashed '.Str::plural($this->module_sub));
    }

    /**
     * @param                        $id
     * @param                        $status
     *
     * @return mixed
     * @throws GeneralException
     */
    public function mark(Request $request, $id, $status)
    {
        $this->repository->mark($id, (int) $status);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete(Request $request, $id)
    {
        $this->repository->forceDelete($id);

        return redirect()->route($this->module_route.'.deleted')->with('flash_success','Record Successfully Deleted Permanently');
    }

    /**

     * @param                        $id
     * @return mixed
     * @throws GeneralException
     */
    public function restore(Request $request, $id)
    {
        $this->repository->restore($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Restored');
    }
}
