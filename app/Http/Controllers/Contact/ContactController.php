<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\BaseController;
use App\Repositories\ContactRepository;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Models\ContactDesignation;
use Throwable;

/**
 * Class ContactController.
 */
class ContactController extends BaseController
{
    private $repository;
    private $contactDocument;
    /**
     * ContactController constructor.
     *
     * @param  ContactRepository  $repository
     */
    public function __construct(ContactRepository $repository)
    {
        $this->repository = $repository;
        $this->module_parent = 'Contact Management';
        $this->module_sub = 'Contact';
        $this->module_name = 'contact';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-contact';
        $this->main_heading = 'Contact Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
        $this->contactDocument = ContactDesignation::all();
    }

    /**
     *
     * @return Factory|View
     */
    public function index()
    {
        return view($this->module_view.'.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active '.Str::plural($this->module_sub));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view($this->module_view.'.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('contactDocument', $this->contactDocument)
            ->with('sub_heading', 'Add New '.Str::Singular($this->module_sub));
    }

    /**
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $this->repository->create($request->except('_method', '_token'));
        return redirect()->route($this->module_route.'.index')
            ->with('flash_success','Record Successfully Saved');
    }

    /**
     * @param                        $id
     * @return mixed
     */
    public function show(Request $request, $id)
    {
        $module = $this->repository->getById($id);
        return view($this->module_view.'.show')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View '.Str::Singular($this->module_sub))
            ->with('module', $module);
    }

    /**
     * @param                        $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('contactDocument', $this->contactDocument)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module);
    }

    /**
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function update(Request $request, $id)
    {
        $this->repository->update($id, $request->except('_method', 'token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy(Request $request, $id)
    {
        $this->repository->destroy($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted');
    }
}
