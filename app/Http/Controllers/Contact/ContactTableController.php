<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\BaseController;
use App\Repositories\ContactRepository;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class ContactTableController.
 */
final class ContactTableController extends BaseController
{
    private $repository;

    /**
     * ContactTableController constructor.
     *
     * @param  ContactRepository  $repository
     */
    public function __construct(ContactRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return mixed
     * @throws \Exception
     */
    public function datatable(Request $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->addIndexColumn()
            ->escapeColumns(['id','designation','name','email','contact_no'])
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->make(true);
    }
}
