<?php

namespace App\Http\Controllers\Sitesection;

use App\Http\Controllers\BaseController;
use App\Repositories\SitesectionRepository;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class SitesectionTableController.
 */
final class SitesectionTableController extends BaseController
{
    private $repository;

    /**
     * SitesectionTableController constructor.
     *
     * @param  SitesectionRepository  $repository
     */
    public function __construct(SitesectionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return mixed
     * @throws \Exception
     */
    public function datatable(Request $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->addIndexColumn()
            ->escapeColumns(['id','section_name'])
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->make(true);
    }
}
