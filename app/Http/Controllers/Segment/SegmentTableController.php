<?php

namespace App\Http\Controllers\Segment;

use App\Http\Controllers\BaseController;
use App\Repositories\SegmentRepository;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class CompanyTableController.
 */
final class SegmentTableController extends BaseController
{
    private $repository;

    /**
     * SegmentTableController constructor.
     *
     * @param  SegmentRepository  $repository
     */
    public function __construct(SegmentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return mixed
     * @throws \Exception
     */
    public function datatable(Request $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->addIndexColumn()
            ->escapeColumns(['id', 'name', 'short_name'])
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->make(true);
    }
}
