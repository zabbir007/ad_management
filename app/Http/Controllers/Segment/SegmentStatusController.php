<?php

namespace App\Http\Controllers\Segment;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Repositories\SegmentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class SegmentStatusController.
 */
class SegmentStatusController extends BaseController
{

    /**
     * @var SegmentRepository
     */
    private $repository;

    /**
     * SegmentStatusController constructor.
     *
     * @param  SegmentRepository  $repository
     */
    public function __construct(SegmentRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Segment Management';
        $this->module_sub = 'Segment';
        $this->module_name = 'segment';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-segment';
        $this->main_heading = 'Segment Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     *
     * @return mixed
     */
    public function getDeactivated()
    {
        return view($this->module_view.'.deactivated')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Deactivated '.Str::plural($this->module_sub));
    }

    /**
     *
     * @return mixed
     */
    public function getDeleted()
    {
        return view($this->module_view.'.trashed')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Trashed '.Str::plural($this->module_sub));
    }

    /**
     * @param                        $id
     * @param                        $status
     *
     * @return mixed
     * @throws GeneralException
     */
    public function mark(Request $request, $id, $status)
    {
        $this->repository->mark($id, (int) $status);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete(Request $request, $id)
    {
        $this->repository->forceDelete($id);

        return redirect()->route($this->module_route.'.deleted')->with('flash_success','Record Successfully Deleted Permanently');
    }

    /**

     * @param                        $id
     * @return mixed
     * @throws GeneralException
     */
    public function restore(Request $request, $id)
    {
        $this->repository->restore($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Restored');
    }
}
