<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Company\ManageCompanyRequest;
use App\Repositories\CompanyRepository;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class CompanyTableController.
 */
final class CompanyTableController extends BaseController
{
    private $repository;

    /**
     * CompanyTableController constructor.
     *
     * @param  CompanyRepository  $repository
     */
    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  ManageCompanyRequest  $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function datatable(ManageCompanyRequest $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->addIndexColumn()
            ->escapeColumns(['id', 'name', 'short_name'])
            ->addColumn('company_logo', function ($q) {
                return \Html::img($q->company_logo, $q->name)->attributes(['height' => '30px']);
            })
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->rawColumns(['company_logo'])
            ->make(true);
    }
}
