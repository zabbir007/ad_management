<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Company\CreateCompanyRequest;
use App\Http\Requests\Company\DestroyCompanyRequest;
use App\Http\Requests\Company\EditCompanyRequest;
use App\Http\Requests\Company\ManageCompanyRequest;
use App\Http\Requests\Company\ShowCompanyRequest;
use App\Http\Requests\Company\StoreCompanyRequest;
use App\Http\Requests\Company\UpdateCompanyRequest;
use App\Repositories\CompanyRepository;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

/**
 * Class CompanyController.
 */
class CompanyController extends BaseController
{
    private $repository;

    /**
     * CompanyController constructor.
     *
     * @param  CompanyRepository  $repository
     */
    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Company Management';
        $this->module_sub = 'Company';
        $this->module_name = 'company';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-company';
        $this->main_heading = 'Company Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ManageCompanyRequest  $request
     *
     * @return Factory|View
     */
    public function index(ManageCompanyRequest $request)
    {
        return view($this->module_view.'.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active '.Str::plural($this->module_sub));
    }

    /**
     * @param  CreateCompanyRequest  $request
     * @return mixed
     */
    public function create(CreateCompanyRequest $request)
    {
        return view($this->module_view.'.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New '.Str::Singular($this->module_sub));
    }

    /**
     * @param  StoreCompanyRequest  $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(StoreCompanyRequest $request)
    {
        $this->repository->create($request->except('_method', '_token'));

        return redirect()->route($this->module_route.'.index')
            ->with('flash_success','Record Successfully Saved');
    }

    /**
     * @param  ShowCompanyRequest  $request
     * @param                        $id
     * @return mixed
     */
    public function show(ShowCompanyRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.show')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View '.Str::Singular($this->module_sub))
            ->with('module', $module);
    }

    /**
     * @param  EditCompanyRequest  $request
     * @param                        $id
     * @return mixed
     */
    public function edit(EditCompanyRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module);
    }

    /**
     * @param  UpdateCompanyRequest  $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        $this->repository->update($id, $request->except('_method', 'token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DestroyCompanyRequest  $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy(DestroyCompanyRequest $request, $id)
    {
        $this->repository->destroy($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted');
    }
}
