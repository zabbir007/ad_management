<?php

namespace App\Http\Controllers\Adsize;

use App\Http\Controllers\BaseController;
use App\Repositories\AdsizeRepository;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class AdsizeTableController.
 */
final class AdsizeTableController extends BaseController
{
    private $repository;

    /**
     * AdsizeTableController constructor.
     *
     * @param  AdsizeRepository  $repository
     */
    public function __construct(AdsizeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return mixed
     * @throws \Exception
     */
    public function datatable(Request $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->addIndexColumn()
            ->escapeColumns(['id','size'])
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->make(true);
    }
}
