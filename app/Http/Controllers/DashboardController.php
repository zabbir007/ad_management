<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class DashboardController.
 */
class DashboardController extends BaseController
{
    /**
     * @return View
     */
    public function index()
    {
       // dump(\Session::all());
        return view('dashboard');
    }
}
