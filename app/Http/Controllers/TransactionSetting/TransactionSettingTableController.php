<?php

namespace App\Http\Controllers\TransactionSetting;

use App\Http\Controllers\BaseController;
use App\Repositories\TransactionSettingRepository;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class TransactionSettingTableController
 *
 * @package App\Http\Controllers\Backend\System\TransactionSetting
 */
final class TransactionSettingTableController extends BaseController
{
    /**
     * TransactionSettingTableController constructor.
     *
     * @param  TransactionSettingRepository  $repository
     */
    public function __construct(TransactionSettingRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function __invoke(Request $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable())
            ->addIndexColumn()
            ->addColumn('branch_name', function ($q) {
                return $q->branch->name;
            })
            ->addColumn('user_name', function ($q) {
                return $q->user->first_name.' '.$q->user->last_name;
            })
            ->addColumn('allowed_days', function ($q) {
                return ($q->isPrevious() ? 'Previous' : 'Next').' '.$q->interval.' '.($q->day == 'd' ? 'Days' : 'Months');
            })
            ->editColumn('expire_at', function ($q) {
                return $q->expire_at->format('d M Y h:i a');
            })
            ->addColumn('expire_label', function ($q) {
                return $q->expire_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->action_buttons;
            })
            ->setRowClass(function ($q) {
                return $q->isExpired() ? 'bg-danger-light' : 'bg-success-light';
            })
            ->rawColumns(['actions', 'expire_label'])
            ->make();
    }
}
