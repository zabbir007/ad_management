<?php

namespace App\Http\Controllers\Auth\Permission;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Permission\ManagePermissionRequest;
use App\Repositories\PermissionRepository;
use DataTables;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class PermissionTableController
 *
 * @package App\Http\Controllers\Auth\Permission
 */
final class PermissionTableController extends BaseController
{
    /**
     * @var $repository
     */
    protected $repository;

    /**
     * PermissionTableController constructor.
     *
     * @param  PermissionRepository  $repository
     */
    public function __construct(PermissionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  ManagePermissionRequest  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function datatable(ManagePermissionRequest $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->escapeColumns(['name','guard_name'])
            ->editColumn('name', function ($q) {
                return Str::title(str_replace('-', ' ', $q->name));
            })
            ->addColumn('identifier', function ($q) {
                return $q->name;
            })
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->removeColumn(['users', 'permissions'])
            ->make(true);
    }
}
