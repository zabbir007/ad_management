<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class PasswordExpiredController.
 */
class PasswordExpiredController extends Controller
{
    /**
     * @return Factory|View
     */
    public function expired()
    {
        abort_unless(config('access.users.password_expires_days'), 404);

        return view('.auth.passwords.expired');
    }

    /**
     * @param  UpdatePasswordRequest  $request
     * @param  UserRepository         $userRepository
     *
     * @return mixed
     * @throws GeneralException
     */
    public function update(UpdatePasswordRequest $request, UserRepository $userRepository)
    {
        abort_unless(config('access.users.password_expires_days'), 404);

        $userRepository->updatePassword($request->only('old_password', 'password'), true);

        return redirect()->route('.user.account')
            ->with('flash_success',__('strings..user.password_updated'));
    }
}
