<?php

namespace App\Http\Controllers\Auth\User;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\User\DestroyUserRequest;
use App\Http\Requests\User\EditUserRequest;
use App\Http\Requests\User\ManageUserProfileRequest;
use App\Http\Requests\User\ManageUserRequest;
use App\Http\Requests\User\ShowUserRequest;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

/**
 * Class UserController.
 */
class UserController extends BaseController
{
    /**
     * @var $repository
     */
    protected $repository;

    /**
     * UserController constructor.
     *
     * @param  UserRepository  $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Access Manager';
        $this->module_sub = 'User';
        $this->module_name = 'user';
        $this->module_icon = 'fas fa-users';
        $this->module_permission = 'setup-user';
        $this->main_heading = 'User Management';
        $this->module_title = $this->module_parent . ' - ' . app_name();
        $this->page_heading = $this->main_heading.' - '. app_name();
        $this->sub_heading = '';
        $this->module_route = $this->module_name;
        $this->module_view = 'auth.'.$this->module_name;
    }

    /**
     * @param  ManageUserRequest  $request
     *
     * @return Factory|View
     */
    public function index(ManageUserRequest $request)
    {
        return view($this->module_view.'.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active '.Str::plural($this->module_sub));
    }

    /**
     * @param  ManageUserRequest  $request
     * @param  RoleRepository  $roleRepository
     * @param  PermissionRepository  $permissionRepository
     *
     * @return mixed
     */
    public function create(
        ManageUserRequest $request,
        RoleRepository $roleRepository,
        PermissionRepository $permissionRepository
    ) {
        return view($this->module_view.'.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New '.Str::Singular($this->module_sub))
            ->with('roles', $roleRepository->with('permissions')->get(['id', 'name']))
            ->with('permissions', $permissionRepository->get(['id', 'name']));
    }

    /**
     * @param  StoreUserRequest  $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(StoreUserRequest $request)
    {
        $this->repository->create($request->except(['_token']));

        return redirect()->route($this->module_route.'.index')
            ->with('flash_success', 'Record Successfully Saved');
    }

    /**
     * @param  ShowUserRequest  $request
     * @param  RoleRepository  $roleRepository
     * @param  PermissionRepository  $permissionRepository
     * @param                     $id
     * @return mixed
     */
    public function show(
        ShowUserRequest $request,
        RoleRepository $roleRepository,
        PermissionRepository $permissionRepository,
        $id
    ) {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.show')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('roles', $roleRepository->get())
            ->with('user_roles', $module->roles->pluck('name')->all())
            ->with('permissions', $permissionRepository->get(['id', 'name']))
            ->with('user_permissions', $module->permissions->pluck('name')->all());
    }

    /**
     * @param  EditUserRequest  $request
     * @param  RoleRepository  $roleRepository
     * @param  PermissionRepository  $permissionRepository
     * @param                        $id
     * @return mixed
     */
    public function edit(
        EditUserRequest $request,
        RoleRepository $roleRepository,
        PermissionRepository $permissionRepository,
        $id
    ) {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('roles', $roleRepository->get())
            ->with('userRoles', $module->roles->pluck('name')->all())
            ->with('permissions', $permissionRepository->get(['id', 'name']))
            ->with('userPermissions', $module->permissions->pluck('name')->all());

    }

    /**
     * @param  UpdateUserRequest  $request
     * @param                     $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $this->repository->update($id, $request->only(
            'first_name',
            'last_name',
            'email',
            'username',
            'roles',
            'permissions'
        ));

        return redirect()->route($this->module_route.'.index')
            ->with('flash_success', 'Record Successfully Updated');
    }

    /**
     * @param  DestroyUserRequest  $request
     * @param                     $id
     * @return mixed
     * @throws Exception
     */
    public function destroy(DestroyUserRequest $request, $id)
    {
        $this->repository->deleteById($id);

        return redirect()->route($this->module_route.'.deleted')
            ->with('flash_success', 'Record Successfully Deleted');
    }

    /**
     * @param  ManageUserProfileRequest  $request
     * @param                            $id
     * @return Factory|View
     */
    public function profile(ManageUserProfileRequest $request, $id)
    {
        $nameTitles = collect([
            'Mr.' => 'Mr.',
            'Mrs' => 'Mrs',
            'Miss' => 'Miss',
        ]);

        $module = $this->repository->getById($id)->load('employee');

        return view($this->module_view.'.profile')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('titles', $nameTitles);
    }
}
