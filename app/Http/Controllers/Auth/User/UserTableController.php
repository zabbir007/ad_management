<?php

namespace App\Http\Controllers\Auth\User;

use App\Http\Controllers\BaseController;
use App\Repositories\UserRepository;
use DataTables;
use Exception;
use Illuminate\Http\Request;

/**
 * Class UserTableController.
 */
final class UserTableController extends BaseController
{
    /**
     * @var $repository
     */
    protected $repository;

    /**
     * UserTableController constructor.
     *
     * @param  UserRepository  $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function datatable(Request $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed'), $request->get('roles')))
            ->rawColumns(['roles_label', 'status_label'])
            ->escapeColumns(['first_name', 'last_name', 'email', 'username'])
            ->editColumn('confirmed', function ($q) {
                return $q->confirmed_label;
            })
            ->addColumn('fullname', function ($q) {
                return $q->fullname;
            })
            ->addColumn('username', function ($q) {
                return $q->username;
            })
            ->addColumn('roles_label', function ($q) {
                return $q->roles_label;
            })
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->setRowClass(function ($q) {
                return !$q->isConfirmed() && config('access.users.requires_approval') ? 'danger' : '';
            })
            ->make(true);
    }
}
