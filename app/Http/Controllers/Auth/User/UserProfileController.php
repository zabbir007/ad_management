<?php

namespace App\Http\Controllers\Auth\User;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\ManageUserProfileRequest;
use App\Http\Requests\User\ManageUserRequest;
use App\Http\Requests\User\UpdateProfileRequest;
use App\Http\Requests\User\UpdateUserPasswordRequest;
use App\Repositories\UserRepository;
use Illuminate\Support\Str;


/**
 * Class UserProfileController
 *
 * @package App\Http\Controllers\Auth\User
 */
class UserProfileController extends BaseController
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * ProfileController constructor.
     *
     * @param  UserRepository  $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->repository = $userRepository;

        $this->module_parent = 'User Management';
        $this->module_sub = 'User';
        $this->module_name = 'user';
        $this->module_icon = 'fas fa-users';
        $this->module_permission = 'setup-user';
        $this->main_heading = 'My Profile';
        $this->module_title = $this->module_parent . ' - ' . app_name();
        $this->page_heading = $this->main_heading.' - '. app_name();
        $this->sub_heading = '';
        $this->module_route = 'auth.'.$this->module_name;
        $this->module_view = 'auth.'.$this->module_name;
    }

    /**
     * @param  ManageUserProfileRequest  $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function show(ManageUserProfileRequest $request)
    {
        $user = $this->repository->getById(auth()->id());

        return view($this->module_view.'.profile')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'My Profile')
            ->with('module', $user);
    }

    /**
     * @param  ManageUserProfileRequest  $request
     *
     * @return mixed
     */
    public function edit(ManageUserProfileRequest $request)
    {
        $user = $this->repository->getById(auth()->id());

        return view($this->module_view.'.update-profile')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $user);
    }

    /**
     * @param  UpdateProfileRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     */
    public function update(UpdateProfileRequest $request)
    {
        $output = $this->repository->update(
            $request->user()->id,
            $request->only('first_name', 'last_name', 'email', 'avatar_type', 'avatar_location'),
            $request->has('avatar_location') ? $request->file('avatar_location') : false
        );

        // E-mail address was updated, user has to reconfirm
        if (is_array($output) && $output['email_changed']) {
            auth()->logout();

            return redirect()->route('auth.login')->withFlashInfo(__('strings..user.email_changed_notice'));
        }

        return redirect()->route('user.account')->with('flash_success',__('strings..user.profile_updated'));
    }

    /**
     * @param  ManageUserRequest  $request
     * @return mixed
     */
    public function showPassword(ManageUserRequest $request)
    {
        $user = $this->repository->getById(auth()->id());

        return view($this->module_view.'.show-change-password')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Change My Password')
            ->with('module', $user);
    }

    /**
     * @param  UpdateUserPasswordRequest  $request
     * @param                             $id
     * @return mixed
     * @throws GeneralException
     */
    public function postPassword(UpdateUserPasswordRequest $request, $id)
    {
        $this->repository->updatePassword($id, $request->only('password'));

        return redirect()->route($this->module_route.'.index')->with('flash_success',__('alerts.users.updated_password'));
    }
}
