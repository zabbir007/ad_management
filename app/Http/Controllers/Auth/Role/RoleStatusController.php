<?php

namespace App\Http\Controllers\Auth\Role;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Role\ManageRoleRequest;
use App\Http\Requests\Role\ViewDeactivedRoleRequest;
use App\Http\Requests\Role\ViewDeletedRoleRequest;
use App\Http\Requests\User\ManageUserRequest;
use App\Repositories\RoleRepository;
use Str;


class RoleStatusController extends BaseController
{
    protected $repository;

    /**
     * @param  RoleRepository  $repository
     */
    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Access Manager';
        $this->module_sub = 'Role';
        $this->module_name = 'role';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-user';
        $this->main_heading = 'Role Manager';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = $this->module_name;
        $this->module_view = 'auth.'.$this->module_name;
    }

    /**
     * @param  ViewDeactivedRoleRequest  $request
     *
     * @return mixed
     */
    public function getDeactivated(ViewDeactivedRoleRequest $request)
    {
        return view($this->module_view.'.deactivated')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Deactivated '.Str::plural($this->module_sub));
    }

    /**
     * @param  ViewDeletedRoleRequest  $request
     *
     * @return mixed
     */
    public function getDeleted(ViewDeletedRoleRequest $request)
    {
        return view($this->module_view.'.trashed')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Trashed '.Str::plural($this->module_sub));
    }

    /**
     * @param  ManageRoleRequest  $request
     * @param                     $id
     * @param                     $status
     *
     * @return mixed
     * @throws GeneralException
     */
    public function mark(ManageRoleRequest $request, $id, $status)
    {
        $this->repository->mark($id, (int) $status);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Updated');

    }
}
