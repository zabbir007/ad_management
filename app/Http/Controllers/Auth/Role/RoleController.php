<?php

namespace App\Http\Controllers\Auth\Role;

use App\Events\Role\RoleDeleted;
use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Role\DestroyRoleRequest;
use App\Http\Requests\Role\EditRoleRequest;
use App\Http\Requests\Role\ManageRoleRequest;
use App\Http\Requests\Role\ShowRoleRequest;
use App\Http\Requests\Role\StoreRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Exception;
use Str;
use Throwable;

/**
 * Class RoleController.
 */
class RoleController extends BaseController
{
    /**
     * @var RoleRepository
     */
    protected $repository;

    /**
     * @var PermissionRepository
     */
    protected $permissionRepository;

    /**
     * @param  RoleRepository  $repository
     * @param  PermissionRepository  $permissionRepository
     */
    public function __construct(RoleRepository $repository, PermissionRepository $permissionRepository)
    {
        $this->repository = $repository;
        $this->permissionRepository = $permissionRepository;

        $this->module_parent = 'Access Manager';
        $this->module_sub = 'Role';
        $this->module_name = 'role';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-user';
        $this->main_heading = 'Role Manager';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = $this->module_name;
        $this->module_view = 'auth.'.$this->module_name;
    }

    /**
     * @param  ManageRoleRequest  $request
     *
     * @return mixed
     */
    public function index(ManageRoleRequest $request)
    {
        return view($this->module_view.'.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active '.Str::plural($this->module_sub));
    }

    /**
     * @param  ManageRoleRequest  $request
     *
     * @return mixed
     */
    public function create(ManageRoleRequest $request)
    {
        return view($this->module_view.'.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New '.Str::Singular($this->module_sub))
            ->with('permissions', $this->permissionRepository->get(['id', 'name']));
    }

    /**
     * @param  StoreRoleRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function store(StoreRoleRequest $request)
    {
        $this->repository->create($request->only('name', 'associated-permissions', 'permissions', 'sort'));

        return redirect()->route($this->module_route.'.index')
            ->with('flash_success', 'Record Successfully Saved');
    }

    /**
     * @param  ShowRoleRequest  $request
     * @param                     $id
     * @return mixed
     */
    public function show(ShowRoleRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.show')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('permissions', $this->permissionRepository->get())
            ->with('RolePermissions', $module->permissions->pluck('name')->all());
    }

    /**
     * @param  EditRoleRequest  $request
     * @param                     $id
     * @return mixed
     */
    public function edit(EditRoleRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        if ($module->isSuperAdmin()) {
            return redirect()->route($this->module_route.'.index')
                ->with('flash_danger', 'You can not edit the super administrator role.');
        }

        return view($this->module_view.'.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('rolePermissions', $module->permissions->pluck('name')->all())
            ->with('permissions', $this->permissionRepository->get());
    }

    /**
     * @param  UpdateRoleRequest  $request
     * @param                     $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function update(UpdateRoleRequest $request, $id)
    {
        $this->repository->update($id, $request->only('name', 'permissions'));

        return redirect()->route($this->module_route.'.index')->with('flash_success', 'Record Successfully Updated');
    }

    /**
     * @param  DestroyRoleRequest  $request
     * @param                     $id
     * @return mixed
     * @throws Exception
     */
    public function destroy(DestroyRoleRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        if ($module->isSuperAdmin()) {
            return redirect()->route($this->module_route.'.index')
                ->with('flash_danger', __('exceptions..access.roles.cant_delete_admin'));
        }

        $this->repository->deleteById($module->id);

        event(new RoleDeleted($module));

        return redirect()->route($this->module_route.'.index')->with('flash_success', __('alerts..roles.deleted'));
    }
}
