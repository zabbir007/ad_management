<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Str;

/**
 * Class PassportAccessController
 *
 * @package App\Http\Controllers\Auth
 */
class PassportAccessController extends BaseController
{
    /**
     */
    public function __construct()
    {
        $this->module_parent = 'Access Management';
        $this->module_sub = 'Access';
        $this->module_name = 'access';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_route = 'auth.'.$this->module_name;
        $this->module_view = '.auth.'.$this->module_name;
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     */
    public function clients(Request $request)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = Str::singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'clients';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $main_heading = $this->module_parent;
        $sub_heading = 'All '.Str::plural($this->module_sub);
        $page_heading = app_name().' | '.$main_heading.' | '.$sub_heading;

        return view($module_view.'.'.$module_action,
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route', 'main_heading', 'sub_heading')
        );
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tokens(Request $request)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = Str::singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'tokens';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $main_heading = $this->module_parent;
        $sub_heading = 'All '.Str::plural($this->module_sub);
        $page_heading = app_name().' | '.$main_heading.' | '.$sub_heading;

        return view($module_view.'.'.$module_action,
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route', 'main_heading', 'sub_heading')
        );
    }
}
