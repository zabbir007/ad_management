<?php

namespace App\Http\Composers;

use App\Repositories\TransactionSettingRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\View\View;

/**
 * Class GlobalComposer.
 */
class GlobalComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     */
    public function compose(View $view)
    {
        $view->with('logged_in_user', auth()->user());
        $view->with('logged_in_user_branches', Collection::make(\Session::get('user_branches')));

        $enabledDates = app(TransactionSettingRepository::class)->getPermittedDays();
        // now()->format('d-m-Y');

        $view->with('enable_dates', $enabledDates);
    }
}
