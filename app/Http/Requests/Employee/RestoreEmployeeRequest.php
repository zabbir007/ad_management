<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class RestoreEmployeeRequest
 *
 * @package App\Http\Requests\Employee
 */
class RestoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-employee-restore');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
