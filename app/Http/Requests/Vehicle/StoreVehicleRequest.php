<?php

namespace App\Http\Requests\Vehicle;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StoreVehicleRequest.
 */
class StoreVehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-vehicle-store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required'],
            'name' => ['required'],
            'vehicle_type' => ['required'],
            'manufacturer' => ['required'],
            'manufacturer_year' => ['required'],
            'license_plate_no' => ['required'],
            'license_year' => ['required'],
            'chassis_no' => ['required'],
            'engine_no' => ['required'],
            'main_tank_fuel_id' => ['required'],
            'main_tank_fuel_capacity' => ['required'],
        ];
    }
}
