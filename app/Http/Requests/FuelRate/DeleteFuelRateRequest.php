<?php

namespace App\Http\Requests\FuelRate;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DeleteFuelRateRequest
 *
 * @package App\Http\Requests\FuelRate
 */
class DeleteFuelRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('transactions-fuel-rate-delete');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
