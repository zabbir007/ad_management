<?php

namespace App\Http\Requests\FuelRate;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class ShowFuelRateRequest
 *
 * @package App\Http\Requests\FuelRate
 */
class ShowFuelRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('transactions-fuel-rate-show');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
