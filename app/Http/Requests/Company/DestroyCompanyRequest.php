<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DestroyCompanyRequest
 *
 * @package App\Http\Requests\Company
 */
class DestroyCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-company-destroy');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
