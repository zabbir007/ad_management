<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DeleteCompanyRequest
 *
 * @package App\Http\Requests\Company
 */
class DeleteCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-company-delete');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
