<?php

namespace App\Http\Requests\Branch;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class RestoreBranchRequest
 *
 * @package App\Http\Requests\Branch
 */
class RestoreBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-branch-restore');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
