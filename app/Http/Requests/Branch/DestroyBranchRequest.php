<?php

namespace App\Http\Requests\Branch;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DestroyBranchRequest
 *
 * @package App\Http\Requests\Branch
 */
class DestroyBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-branch-destroy');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
