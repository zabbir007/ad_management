<?php

namespace App\Http\Requests\Branch;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DeleteBranchRequest
 *
 * @package App\Http\Requests\Branch
 */
class DeleteBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-branch-delete');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
