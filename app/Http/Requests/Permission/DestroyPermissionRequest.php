<?php

namespace App\Http\Requests\Permission;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DestroyPermissionRequest
 *
 * @package App\Http\Requests\Permission
 */
class DestroyPermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-permission-destroy');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
