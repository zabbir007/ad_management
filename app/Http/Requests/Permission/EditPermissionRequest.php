<?php

namespace App\Http\Requests\Permission;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class EditPermissionRequest
 *
 * @package App\Http\Requests\Permission
 */
class EditPermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-permission-edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
