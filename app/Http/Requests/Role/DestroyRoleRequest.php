<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DestroyRoleRequest
 *
 * @package App\Http\Requests\Role
 */
class DestroyRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-role-destroy');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
