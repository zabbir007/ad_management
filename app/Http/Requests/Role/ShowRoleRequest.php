<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class ShowRoleRequest
 *
 * @package App\Http\Requests\Role
 */
class ShowRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-role-show');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
