<?php

namespace App\Http\Requests\VehicleDriver;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ManageVehicleDriverRequest.
 */
class ManageVehicleDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-vehicle-driver-index');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
