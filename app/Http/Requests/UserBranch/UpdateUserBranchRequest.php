<?php

namespace App\Http\Requests\UserBranch;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserBranchRequest.
 */
class UpdateUserBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-branch-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['required'],
            'branch_ids' => ['required']
        ];
    }
}
