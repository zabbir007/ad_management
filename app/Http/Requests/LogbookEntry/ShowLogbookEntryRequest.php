<?php

namespace App\Http\Requests\LogbookEntry;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class ShowLogbookEntryRequest
 *
 * @package App\Http\Requests\LogbookEntry
 */
class ShowLogbookEntryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('transactions-logbook-entry-show');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
