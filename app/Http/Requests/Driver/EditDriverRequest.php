<?php

namespace App\Http\Requests\Driver;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class EditDriverRequest
 *
 * @package App\Http\Requests\Driver
 */
class EditDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-driver-edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
