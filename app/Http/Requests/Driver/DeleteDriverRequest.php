<?php

namespace App\Http\Requests\Driver;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DeleteDriverRequest
 *
 * @package App\Http\Requests\Driver
 */
class DeleteDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-driver-delete');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
