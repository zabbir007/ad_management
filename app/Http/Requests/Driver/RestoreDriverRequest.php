<?php

namespace App\Http\Requests\Driver;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class RestoreDriverRequest
 *
 * @package App\Http\Requests\Driver
 */
class RestoreDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-driver-restore');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
