<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class SetSessionData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()){
            if (!\Session::has('session_date')){
                \Session::put('session_date', Carbon::now()->toDateString());
            }

            if (!\Session::has('user_branches')){
                $user = request()->user();

                //store user branches to session
                $branches = $user->isUser() ? $user->branches->toArray() : '*';

                \Session::put('user_branches', $branches);
            }
        }

        return $next($request);
    }
}
