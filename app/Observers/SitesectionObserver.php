<?php

namespace App\Observers;


use App\Models\Sitesection;
use Carbon\Carbon;

/**
 * Class SitesectionObserver.
 */
class SitesectionObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $model->created_by = auth()->id() ?? 1;
        $model->created_at = Carbon::now();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  Sitesection  $model
     */
    public function created(Sitesection $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
        $model->updated_at = Carbon::now();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  Sitesection  $model
     */
    public function updated(Sitesection $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Sitesection  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
        $model->deleted_at = Carbon::now();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Sitesection  $model
     */
    public function deleted(Sitesection $model): void
    {

    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  Sitesection  $model
     */
    public function restored(Sitesection $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  Sitesection  $model
     */
    public function forceDeleted(Sitesection $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
