<?php

namespace App\Observers;


use App\Models\Company;
use Carbon\Carbon;

/**
 * Class CompanyObserver.
 */
class CompanyObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $model->created_by = auth()->id() ?? 1;
        $model->created_at = Carbon::now();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  Company  $model
     */
    public function created(Company $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
        $model->updated_at = Carbon::now();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  Company  $model
     */
    public function updated(Company $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Company  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
        $model->deleted_at = Carbon::now();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Company  $model
     */
    public function deleted(Company $model): void
    {

    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  Company  $model
     */
    public function restored(Company $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  Company  $model
     */
    public function forceDeleted(Company $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
