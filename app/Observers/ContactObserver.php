<?php

namespace App\Observers;


use App\Models\Contact;
use Carbon\Carbon;

/**
 * Class ContactObserver.
 */
class ContactObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $model->created_by = auth()->id() ?? 1;
        $model->created_at = Carbon::now();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  Contact  $model
     */
    public function created(Contact $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
        $model->updated_at = Carbon::now();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  Contact  $model
     */
    public function updated(Contact $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Contact  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
        $model->deleted_at = Carbon::now();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Contact  $model
     */
    public function deleted(Contact $model): void
    {

    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  Contact  $model
     */
    public function restored(Contact $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  Contact  $model
     */
    public function forceDeleted(Contact $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
