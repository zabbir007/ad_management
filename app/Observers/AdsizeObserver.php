<?php

namespace App\Observers;


use App\Models\Adsize;
use Carbon\Carbon;

/**
 * Class AdsizeObserver.
 */
class AdsizeObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $model->created_by = auth()->id() ?? 1;
        $model->created_at = Carbon::now();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  Adsize  $model
     */
    public function created(Adsize $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
        $model->updated_at = Carbon::now();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  Adsize  $model
     */
    public function updated(Adsize $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Adsize  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id() ?? 1;
        $model->deleted_at = Carbon::now();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Adsize  $model
     */
    public function deleted(Adsize $model): void
    {

    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  Adsize  $model
     */
    public function restored(Adsize $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  Adsize  $model
     */
    public function forceDeleted(Adsize $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
