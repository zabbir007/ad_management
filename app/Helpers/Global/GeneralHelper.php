<?php

if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('home_route')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function home_route()
    {
        if (auth()->check()) {
            if (auth()->user()->can('view backend')) {
                return 'dashboard';
            }

            return 'dashboard';
        }

        return 'auth.login';
    }
}

if (!function_exists('settings')) {
    /**
     * @param $value
     * @return string
     */
    function settings($key)
    {
//        $value = (float)$value;
//
//        $currency = get_currency();
//        $position = Settings::get('financial.currency_position');
//        $format = Settings::get('financial.money_format');
//        $decimals = Settings::get('financial.decimals');
//
//        $money = '';
//
//        if ($position === '1') {
//            $money .= $currency . ' ';
//        }
////        elseif ($position == "2") $money .= $currency;
//
//        if ($format === '2') {
//            $thousandSep = '.';
//            $decimalPoint = ',';
//        } elseif ($format === '3') {
//            $thousandSep = '';
//            $decimalPoint = '.';
//        } elseif ($format === '4') {
//            $thousandSep = '';
//            $decimalPoint = ',';
//        } else {
//            $thousandSep = ',';
//            $decimalPoint = '.';
//        }
//
//        $money .= number_format($value, $decimals, $decimalPoint, $thousandSep);
//
//        if ($position == '3') {
//            $money .= ' ' . $currency;
//        }
//
//        return $money;
    }
}