<?php

namespace App\Listeners;

use App\Events\Permission\PermissionCreated;
use App\Events\Permission\PermissionDeleted;
use App\Events\Permission\PermissionUpdated;
use Illuminate\Events\Dispatcher;

/**
 * Class PermissionEventListener.
 */
class PermissionEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Permission Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Permission Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Permission Deleted');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            PermissionCreated::class,
            'App\Listeners\PermissionEventListener@onCreated'
        );

        $events->listen(
            PermissionUpdated::class,
            'App\Listeners\PermissionEventListener@onUpdated'
        );

        $events->listen(
            PermissionDeleted::class,
            'App\Listeners\PermissionEventListener@onDeleted'
        );
    }
}
