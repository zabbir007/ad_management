<?php

namespace App\Listeners;

use App\Events\Adsize\AdsizeCreated;
use App\Events\Adsize\AdsizeDeleted;
use App\Events\Adsize\AdsizeUpdated;
use App\Events\Adsize\AdsizeRestored;
use App\Events\Adsize\AdsizeDeactivated;
use App\Events\Adsize\AdsizeReactivated;
use App\Events\Adsize\AdsizePermanentlyDeleted;

/**
 * Class AdsizeEventListener
 *
 * @package App\Listeners
 */
class AdsizeEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Adsize Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Adsize Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Adsize Deleted');
    }

    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('Adsize Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('Adsize Reactivated');
    }


    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('Adsize Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('Adsize Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            AdsizeCreated::class,
            'App\Listeners\AdsizeEventListener@onCreated'
        );

        $events->listen(
            AdsizeUpdated::class,
            'App\Listeners\AdsizeEventListener@onUpdated'
        );

        $events->listen(
            AdsizeDeleted::class,
            'App\Listeners\AdsizeEventListener@onDeleted'
        );

        $events->listen(
            AdsizeDeactivated::class,
            'App\Listeners\AdsizeEventListener@onDeactivated'
        );

        $events->listen(
            AdsizeReactivated::class,
            'App\Listeners\AdsizeEventListener@onReactivated'
        );

        $events->listen(
            AdsizePermanentlyDeleted::class,
            'App\Listeners\AdsizeEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            AdsizeRestored::class,
            'App\Listeners\AdsizeEventListener@onRestored'
        );
    }
}
