<?php

namespace App\Listeners;

use App\Events\Contact\ContactCreated;
use App\Events\Contact\ContactDeleted;
use App\Events\Contact\ContactUpdated;
use App\Events\Contact\ContactRestored;
use App\Events\Contact\ContactDeactivated;
use App\Events\Contact\ContactReactivated;
use App\Events\Contact\ContactPermanentlyDeleted;

/**
 * Class ContactEventListener
 *
 * @package App\Listeners
 */
class ContactEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Contact Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Contact Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Contact Deleted');
    }

    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('Contact Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('Contact Reactivated');
    }


    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('Contact Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('Contact Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            ContactCreated::class,
            'App\Listeners\ContactEventListener@onCreated'
        );

        $events->listen(
            ContactUpdated::class,
            'App\Listeners\ContactEventListener@onUpdated'
        );

        $events->listen(
            ContactDeleted::class,
            'App\Listeners\ContactEventListener@onDeleted'
        );

        $events->listen(
            ContactDeactivated::class,
            'App\Listeners\ContactEventListener@onDeactivated'
        );

        $events->listen(
            ContactReactivated::class,
            'App\Listeners\ContactEventListener@onReactivated'
        );

        $events->listen(
            ContactPermanentlyDeleted::class,
            'App\Listeners\ContactEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            ContactRestored::class,
            'App\Listeners\ContactEventListener@onRestored'
        );
    }
}
