<?php

namespace App\Listeners;

use App\Events\Segment\SegmentCreated;
use App\Events\Segment\SegmentDeleted;
use App\Events\Segment\SegmentUpdated;
use App\Events\Segment\SegmentRestored;
use App\Events\Segment\SegmentDeactivated;
use App\Events\Segment\SegmentReactivated;
use App\Events\Segment\SegmentPermanentlyDeleted;

/**
 * Class SegmentEventListener
 *
 * @package App\Listeners
 */
class SegmentEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Segment Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Segment Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Segment Deleted');
    }

    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('Segment Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('Segment Reactivated');
    }


    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('Segment Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('Segment Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            SegmentCreated::class,
            'App\Listeners\SegmentEventListener@onCreated'
        );

        $events->listen(
            SegmentUpdated::class,
            'App\Listeners\SegmentEventListener@onUpdated'
        );

        $events->listen(
            SegmentDeleted::class,
            'App\Listeners\SegmentEventListener@onDeleted'
        );

        $events->listen(
            SegmentDeactivated::class,
            'App\Listeners\SegmentEventListener@onDeactivated'
        );

        $events->listen(
            SegmentReactivated::class,
            'App\Listeners\SegmentEventListener@onReactivated'
        );

        $events->listen(
            SegmentPermanentlyDeleted::class,
            'App\Listeners\SegmentEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            SegmentRestored::class,
            'App\Listeners\SegmentEventListener@onRestored'
        );
    }
}
