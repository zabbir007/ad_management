<?php

namespace App\Listeners;

use App\Events\Company\CompanyCreated;
use App\Events\Company\CompanyDeleted;
use App\Events\Company\CompanyUpdated;
use App\Events\Company\CompanyRestored;
use App\Events\Company\CompanyDeactivated;
use App\Events\Company\CompanyReactivated;
use App\Events\Company\CompanyPermanentlyDeleted;

/**
 * Class CompanyEventListener
 *
 * @package App\Listeners
 */
class CompanyEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Company Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Company Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Company Deleted');
    }

    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('Company Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('Company Reactivated');
    }


    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('Company Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('Company Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            CompanyCreated::class,
            'App\Listeners\CompanyEventListener@onCreated'
        );

        $events->listen(
            CompanyUpdated::class,
            'App\Listeners\CompanyEventListener@onUpdated'
        );

        $events->listen(
            CompanyDeleted::class,
            'App\Listeners\CompanyEventListener@onDeleted'
        );

        $events->listen(
            CompanyDeactivated::class,
            'App\Listeners\CompanyEventListener@onDeactivated'
        );

        $events->listen(
            CompanyReactivated::class,
            'App\Listeners\CompanyEventListener@onReactivated'
        );

        $events->listen(
            CompanyPermanentlyDeleted::class,
            'App\Listeners\CompanyEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            CompanyRestored::class,
            'App\Listeners\CompanyEventListener@onRestored'
        );
    }
}
