<?php

namespace App\Listeners;

use App\Events\User\UserConfirmed;
use App\Events\User\UserCreated;
use App\Events\User\UserDeactivated;
use App\Events\User\UserDeleted;
use App\Events\User\UserLoggedIn;
use App\Events\User\UserLoggedOut;
use App\Events\User\UserPasswordChanged;
use App\Events\User\UserPermanentlyDeleted;
use App\Events\User\UserReactivated;
use App\Events\User\UserRegistered;
use App\Events\User\UserRestored;
use App\Events\User\UserUnconfirmed;
use App\Events\User\UserUpdated;
use Illuminate\Events\Dispatcher;


/**
 * Class UserEventListener
 *
 * @package App\Listeners
 */
class UserEventListener
{
    /**
     * @param $event
     */
    public function onLoggedIn($event)
    {
        $ip_address = request()->getClientIp();

        // Update the logging in users time & IP
        $event->user->fill([
            'last_login_at' => now()->toDateTimeString(),
            'last_login_ip' => $ip_address,
        ]);

        // Update the timezone via IP address
        $geoip = geoip($ip_address);

        if ($event->user->timezone !== $geoip['timezone']) {
            // Update the users timezone
            $event->user->fill([
                'timezone' => $geoip['timezone'],
            ]);
        }

        $event->user->save();

        logger('User Logged In: '.$event->user->full_name);
    }

    /**
     * @param $event
     */
    public function onLoggedOut($event)
    {
        logger('User Logged Out: '.$event->user->full_name);
    }

    /**
     * @param $event
     */
    public function onRegistered($event)
    {
        logger('User Registered: '.$event->user->full_name);
    }

    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('User Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('User Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('User Deleted');
    }

    /**
     * @param $event
     */
    public function onConfirmed($event)
    {
        logger('User Confirmed: '.$event->user->full_name);
    }

    /**
     * @param $event
     */
    public function onUnconfirmed($event)
    {
        logger('User Unconfirmed');
    }

    /**
     * @param $event
     */
    public function onPasswordChanged($event)
    {
        logger('User Password Changed');
    }

    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('User Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('User Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('User Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('User Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            UserLoggedIn::class,
            'App\Listeners\UserEventListener@onLoggedIn'
        );

        $events->listen(
            UserLoggedOut::class,
            'App\Listeners\UserEventListener@onLoggedOut'
        );

        $events->listen(
            UserRegistered::class,
            'App\Listeners\UserEventListener@onRegistered'
        );

        $events->listen(
            UserCreated::class,
            'App\Listeners\UserEventListener@onCreated'
        );

        $events->listen(
            UserUpdated::class,
            'App\Listeners\UserEventListener@onUpdated'
        );

        $events->listen(
            UserDeleted::class,
            'App\Listeners\UserEventListener@onDeleted'
        );

        $events->listen(
            UserConfirmed::class,
            'App\Listeners\UserEventListener@onConfirmed'
        );

        $events->listen(
            UserUnconfirmed::class,
            'App\Listeners\UserEventListener@onUnconfirmed'
        );

        $events->listen(
            UserPasswordChanged::class,
            'App\Listeners\UserEventListener@onPasswordChanged'
        );

        $events->listen(
            UserDeactivated::class,
            'App\Listeners\UserEventListener@onDeactivated'
        );

        $events->listen(
            UserReactivated::class,
            'App\Listeners\UserEventListener@onReactivated'
        );

        $events->listen(
            UserPermanentlyDeleted::class,
            'App\Listeners\UserEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            UserRestored::class,
            'App\Listeners\UserEventListener@onRestored'
        );
    }
}
