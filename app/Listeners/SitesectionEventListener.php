<?php

namespace App\Listeners;

use App\Events\Sitesection\SitesectionCreated;
use App\Events\Sitesection\SitesectionDeleted;
use App\Events\Sitesection\SitesectionUpdated;
use App\Events\Sitesection\SitesectionRestored;
use App\Events\Sitesection\SitesectionDeactivated;
use App\Events\Sitesection\SitesectionReactivated;
use App\Events\Sitesection\SitesectionPermanentlyDeleted;

/**
 * Class SitesectionEventListener
 *
 * @package App\Listeners
 */
class SitesectionEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Site Section Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Site Section Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Site Section Deleted');
    }

    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('Site Section Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('Site Section Reactivated');
    }


    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('Site Section Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('Site Section Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            SitesectionCreated::class,
            'App\Listeners\SitesectionEventListener@onCreated'
        );

        $events->listen(
            SitesectionUpdated::class,
            'App\Listeners\SitesectionEventListener@onUpdated'
        );

        $events->listen(
            SitesectionDeleted::class,
            'App\Listeners\SitesectionEventListener@onDeleted'
        );

        $events->listen(
            SitesectionDeactivated::class,
            'App\Listeners\SitesectionEventListener@onDeactivated'
        );

        $events->listen(
            SitesectionReactivated::class,
            'App\Listeners\SitesectionEventListener@onReactivated'
        );

        $events->listen(
            SitesectionPermanentlyDeleted::class,
            'App\Listeners\SitesectionEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            SitesectionRestored::class,
            'App\Listeners\SitesectionEventListener@onRestored'
        );
    }
}
