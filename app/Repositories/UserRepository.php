<?php

namespace App\Repositories;

use App\Events\User\UserConfirmed;
use App\Events\User\UserCreated;
use App\Events\User\UserDeactivated;
use App\Events\User\UserPasswordChanged;
use App\Events\User\UserPermanentlyDeleted;
use App\Events\User\UserReactivated;
use App\Events\User\UserRestored;
use App\Events\User\UserUnconfirmed;
use App\Events\User\UserUpdated;
use App\Exceptions\GeneralException;
use App\Models\User;
use App\Notifications\Auth\UserAccountActive;
use App\Notifications\Auth\UserNeedsConfirmation;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  User  $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param $token
     *
     * @return bool|Model
     */
    public function findByPasswordResetToken($token)
    {
        foreach (DB::table(config('auth.passwords.users.table'))->get() as $row) {
            if (password_verify($token, $row->token)) {
                return $this->getByColumn($row->email, 'email');
            }
        }

        return false;
    }

    /**
     * @param $uuid
     *
     * @return mixed
     * @throws GeneralException
     */
    public function findByUuid($uuid)
    {
        $module = $this->model
            ->uuid($uuid)
            ->first();

        if ($module instanceof $this->model) {
            return $module;
        }

        throw new GeneralException(__('exceptions.access.users.not_found'));
    }

    /**
     * @param $code
     *
     * @return mixed
     * @throws GeneralException
     */
    public function findByConfirmationCode($code)
    {
        $module = $this->model
            ->where('confirmation_code', $code)
            ->first();

        if ($module instanceof $this->model) {
            return $module;
        }

        throw new GeneralException(__('exceptions.access.users.not_found'));
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @param        $roles
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false, $roles)
    {
        $dataTableQuery = $this->model::with('roles')
            ->select([
                'id',
                'first_name',
                'last_name',
                'email',
                'username',
                'active',
                'created_at',
                'updated_at',
                'deleted_at',
            ]);
//            ->whereHas('roles', function ($q) use ($roles) {
//                return $q->whereIn('role_id', $roles);
//            });

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        $dataTableQuery = $this->model::with(['roles', 'branches'])
            ->select([
                'id',
                'first_name',
                'last_name',
                'email',
                'username',
                'active',
                'created_at',
                'updated_at',
                'deleted_at',
            ]);
//            ->whereHas('roles', function ($q) use ($roles) {
//                return $q->whereIn('role_id', $roles);
//            });

        return $dataTableQuery->active(true)
            ->get();
    }

    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model::where('id', '<>', 1)
            ->active()
            ->select(['id', 'first_name', 'last_name'])
            ->get()
            ->pluck('full_name', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return User
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): User
    {
        return DB::transaction(function () use ($data) {
            $module = $this->model::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'username' => $data['username'],
                'password' => $data['password'],
                'active' => isset($data['active']) && $data['active'] === '1',
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => isset($data['confirmed']) && $data['confirmed'] === '1',
            ]);

            // See if adding any additional permissions
            if (!isset($data['permissions']) || !count($data['permissions'])) {
                $data['permissions'] = [];
            }

            if ($module) {
                // User must have at least one role
                if (!count($data['roles'])) {
//                    throw new GeneralException(__('exceptions.access.users.role_needed_create'));
                    $module->assignRole(config('access.users.default_role'));
                } else {
                    $module->syncRoles($data['roles']);
                }

                // Add selected roles/permissions
                $module->syncPermissions($data['permissions']);

                //Send confirmation email if requested and account approval is off
                if ($module->confirmed === false && isset($data['confirmation_email']) && !config('access.users.requires_approval')) {
                    $module->notify(new UserNeedsConfirmation($module->confirmation_code));
                }

                event(new UserCreated($module));

                return $module;
            }

            throw new GeneralException(__('exceptions.access.users.create_error'));
        });
    }

    /**
     * @param $id
     * @param  array  $data
     *
     * @param  bool  $image
     * @return User
     * @throws GeneralException
     * @throws Throwable
     */
    public function update($id, array $data, $image = false): User
    {
        $module = $this->getById($id);
        $this->checkUserByEmail($module, $data['email']);

        // See if adding any additional permissions
        if (!isset($data['permissions']) || !count($data['permissions'])) {
            $data['permissions'] = [];
        }

//        // Upload profile image if necessary
//        if ($image) {
//            $module->avatar_location = $image->store('/avatars', 'public');
//        } else {
//            // No image being passed
//            if ($data['avatar_type'] === 'storage') {
//                // If there is no existing image
//                if (auth()->user()->avatar_location === '') {
//                    throw new GeneralException('You must supply a profile image.');
//                }
//            } else {
//                // If there is a current image, and they are not using it anymore, get rid of it
//                if (auth()->user()->avatar_location !== '') {
//                    Storage::disk('public')->delete(auth()->user()->avatar_location);
//                }
//
//                $module->avatar_location = null;
//            }
//        }

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
            ])) {
                // Add selected roles/permissions
                $module->syncRoles($data['roles']);
                $module->syncPermissions($data['permissions']);

                event(new UserUpdated($module));

                return $module;
            }

            throw new GeneralException(__('exceptions.access.users.update_error'));
        });
    }

    /**
     * @param      $id
     * @param      $input
     *
     * @return User
     * @throws GeneralException
     */
    public function updatePassword($id, $input): User
    {
        $module = $this->getById($id);

        if ($module->update(['password' => $input['password']])) {
            event(new UserPasswordChanged($module));

            return $module;
        }

        throw new GeneralException(__('exceptions.access.users.update_password_error'));
    }

    /**
     * @param        $input
     * @param  bool  $expired
     *
     * @return bool
     * @throws GeneralException
     */
    public function updateUserPassword($input, $expired = false)
    {
        $module = $this->getById(auth()->id());

        if (Hash::check($input['old_password'], $module->password)) {
            if ($expired) {
                $module->password_changed_at = now()->toDateTimeString();
            }

            return $module->update(['password' => $input['password']]);
        }

        throw new GeneralException(__('exceptions.auth.password.change_mismatch'));
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return User
     * @throws GeneralException
     */
    public function mark($id, $status): User
    {
        $module = $this->getById($id);

        if ($status === 0 && auth()->id() === $module->id) {
            throw new GeneralException(__('exceptions.access.users.cant_deactivate_self'));
        }

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new UserDeactivated($module));
                break;
            case 1:
                event(new UserReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException(__('exceptions.access.users.mark_error'));
    }

    /**
     * @param  User  $module
     *
     * @return User
     * @throws GeneralException
     */
    public function confirm(User $module): User
    {
        if ($module->confirmed) {
            throw new GeneralException(__('exceptions.access.users.already_confirmed'));
        }

        $module->confirmed = true;
        $confirmed = $module->save();

        if ($confirmed) {
            event(new UserConfirmed($module));

            // Let user know their account was approved
            if (config('access.users.requires_approval')) {
                $module->notify(new UserAccountActive);
            }

            return $module;
        }

        throw new GeneralException(__('exceptions.access.users.cant_confirm'));
    }

    /**
     * @param  User  $module
     *
     * @return User
     * @throws GeneralException
     */
    public function unconfirm(User $module): User
    {
        if (!$module->confirmed) {
            throw new GeneralException(__('exceptions.access.users.not_confirmed'));
        }

        if ($module->id === 1) {
            // Cant un-confirm admin
            throw new GeneralException(__('exceptions.access.users.cant_unconfirm_admin'));
        }

        if ($module->id === auth()->id()) {
            // Cant un-confirm self
            throw new GeneralException(__('exceptions.access.users.cant_unconfirm_self'));
        }

        $module->confirmed = false;
        $unconfirmed = $module->save();

        if ($unconfirmed) {
            event(new UserUnconfirmed($module));

            return $module;
        }

        throw new GeneralException(__('exceptions.access.users.cant_unconfirm'));
    }

    /**
     * @param $id
     * @return User
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): User
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException(__('exceptions.access.users.delete_first'));
        }

        return DB::transaction(function () use ($module) {
            // Delete associated relationships
            $module->passwordHistories()->delete();

            if ($module->forceDelete()) {
                event(new UserPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException(__('exceptions.access.users.delete_error'));
        });
    }

    /**
     * @param $id
     * @return User
     * @throws GeneralException
     */
    public function restore($id): User
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException(__('exceptions.access.users.cant_restore'));
        }

        if ($module->restore()) {
            event(new UserRestored($module));

            return $module;
        }

        throw new GeneralException(__('exceptions.access.users.restore_error'));
    }

    /**
     * @param  User  $module
     * @param        $email
     *
     * @throws GeneralException
     */
    protected function checkUserByEmail($module, $email)
    {
        // Figure out if email is not the same and check to see if email exists
        if ($module->email !== $email && $this->model->where('email', '=', $email)->first()) {
            throw new GeneralException(trans('exceptions.access.users.email_error'));
        }
    }

    /**
     * @param $fullName
     *
     * @return array
     */
    protected function getNameParts($fullName)
    {
        $parts = array_values(array_filter(explode(' ', $fullName)));
        $size = count($parts);
        $result = [];

        if (empty($parts)) {
            $result['first_name'] = null;
            $result['last_name'] = null;
        }

        if (!empty($parts) && $size === 1) {
            $result['first_name'] = $parts[0];
            $result['last_name'] = null;
        }

        if (!empty($parts) && $size >= 2) {
            $result['first_name'] = $parts[0];
            $result['last_name'] = $parts[1];
        }

        return $result;
    }
}
