<?php

namespace App\Repositories;

use App\Events\Contact\ContactCreated;
use App\Events\Contact\ContactDeactivated;
use App\Events\Contact\ContactDeleted;
use App\Events\Contact\ContactPermanentlyDeleted;
use App\Events\Contact\ContactReactivated;
use App\Events\Contact\ContactRestored;
use App\Events\Contact\ContactUpdated;
use App\Exceptions\GeneralException;
use App\Models\Contact;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Storage;
use Throwable;
use Auth;

/**
 * Class ContactRepository.
 */
class ContactRepository extends BaseRepository
{
    /**
     * ContactRepository constructor.
     *
     * @param  Contact  $model
     */
    public function __construct(Contact $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'designation',
                'name',
                'email',
                'contact_no',
                'active',
                'created_at',
                'updated_at'
            ]);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the CompanyGlobalScope trait
        return $dataTableQuery;
    }

    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model
            ->active()
            ->select(['id', 'name'])
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return Contact
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Contact
    {
        return DB::transaction(function () use ($data) {
            $module = $this->model::create([
                'designation' => $data['designation'],
                'name' => $data['name'],
                'email' => $data['email'],
                'contact_no' => $data['contact_no'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);
            if ($module) {
                event(new ContactCreated($module));
                return $module;
            }
            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return Contact
     * @throws Throwable
     */
    public function update($id, array $data): Contact
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'designation' => $data['designation'],
                'name' => $data['name'],
                'email' => $data['email'],
                'contact_no' => $data['contact_no'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

                event(new ContactUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        if ($status === 0 && $module->id === 1) {
            throw new GeneralException('Default Segment can not be deactivated!');
        }

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new ContactDeactivated($module));
                break;
            case 1:
                event(new ContactReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new ContactDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Contact
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Contact
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new ContactPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Contact
     * @throws GeneralException
     */
    public function restore($id): Contact
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new ContactRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
