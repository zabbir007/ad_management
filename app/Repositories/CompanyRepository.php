<?php

namespace App\Repositories;

use App\Events\Company\CompanyCreated;
use App\Events\Company\CompanyDeactivated;
use App\Events\Company\CompanyDeleted;
use App\Events\Company\CompanyPermanentlyDeleted;
use App\Events\Company\CompanyReactivated;
use App\Events\Company\CompanyRestored;
use App\Events\Company\CompanyUpdated;
use App\Exceptions\GeneralException;
use App\Models\Company;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Storage;
use Throwable;

/**
 * Class CompanyRepository.
 */
class CompanyRepository extends BaseRepository
{
    /**
     * CompanyRepository constructor.
     *
     * @param  Company  $model
     */
    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'code',
                'name',
                'short_name',
                'logo',
                'active',
                'created_at',
                'updated_at'
            ]);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the CompanyGlobalScope trait
        return $dataTableQuery;
    }

    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model
            ->active()
            ->select(['id', 'name'])
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return Company
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Company
    {
        return DB::transaction(function () use ($data) {
            $module = $this->model::create([
                'code' => $data['code'],
                'name' => $data['name'],
                'short_name' => $data['short_name'],
                'address' => $data['address'],
                'email_suffix' => $data['email_suffix'],
                'logo' => $data['logo']->getClientOriginalName(),
                'time_zone' => $data['timezone'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);

            if ($module) {
                Storage::disk('public')
                    ->putFileAs('img/logo', $data['logo'],
                        $data['logo']->getClientOriginalName());

                event(new CompanyCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return Company
     * @throws Throwable
     */
    public function update($id, array $data): Company
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'code' => $data['code'],
                'name' => $data['name'],
                'short_name' => $data['short_name'],
                'address' => $data['address'],
                'email_suffix' => $data['email_suffix'],
                'logo' => isset($data['logo']) ? $data['logo']->getClientOriginalName() : $data['logo'],
                'time_zone' => $data['timezone'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {
                if (isset($data['logo'])) {
                    Storage::disk('public')
                        ->putFileAs('img/logo', $data['logo'],
                            $data['logo']->getClientOriginalName());
                }

                event(new CompanyUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        if ($status === 0 && $module->id === 1) {
            throw new GeneralException('Default company can not be deactivated!');
        }

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new CompanyDeactivated($module));
                break;
            case 1:
                event(new CompanyReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new CompanyDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Company
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Company
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new CompanyPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Company
     * @throws GeneralException
     */
    public function restore($id): Company
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new CompanyRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }

    /**
     * @param  Company  $module
     * @param           $email
     *
     * @throws GeneralException
     */
    protected function checkCompanyByEmail(Company $module, $email)
    {
        // Figure out if email is not the same and check to see if email exists
        if ($module->email !== $email && $this->model->where('email', '=', $email)->first()) {
            throw new GeneralException(trans('exceptions.access.users.email_error'));
        }
    }
}
