<?php

namespace App\Repositories;

use App\Events\Role\RoleCreated;
use App\Events\Role\RoleDeactivated;
use App\Events\Role\RoleReactivated;
use App\Events\Role\RoleUpdated;
use App\Exceptions\GeneralException;
use App\Models\Role;
use Illuminate\Support\Facades\DB;
use Throwable;
use function count;

/**
 * Class RoleRepository.
 */
class RoleRepository extends BaseRepository
{
    /**
     * RoleRepository constructor.
     *
     * @param  Role  $module
     */
    public function __construct(Role $module)
    {
        $this->model = $module;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model::with('Users', 'permissions')
            ->select([
                'id',
                'name',
                'guard_name',
                'active',
                'created_at',
                'updated_at'
            ]);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }


    /**
     * @param  array  $data
     *
     * @return Role
     * @throws Throwable
     * @throws GeneralException
     */
    public function create(array $data): Role
    {
        // Make sure it doesn't already exist
        if ($this->roleExists($data['name'])) {
            throw new GeneralException('A role already exists with the name '.e($data['name']));
        }

        if (!isset($data['permissions']) || !count($data['permissions'])) {
            $data['permissions'] = [];
        }

        //See if the role must contain a permission as per config
        if (config('access.roles.role_must_contain_permission') && count($data['permissions']) === 0) {
            throw new GeneralException(__('exceptions.access.roles.needs_permission'));
        }

        return DB::transaction(function () use ($data) {
            $module = $this->model::create(['name' => strtolower($data['name'])]);

            if ($module) {
                $module->givePermissionTo($data['permissions']);

                event(new RoleCreated($module));

                return $module;
            }

            throw new GeneralException(trans('exceptions.access.roles.create_error'));
        });
    }

    /**
     * @param $id
     * @param  array  $data
     *
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function update($id, array $data)
    {
        $module = $this->getById($id);

        if ($module->isSuperAdmin()) {
            throw new GeneralException('You can not edit the administrator role.');
        }

        // If the name is changing make sure it doesn't already exist
        if ($module->name !== strtolower($data['name'])) {
            if ($this->roleExists($data['name'])) {
                throw new GeneralException('A role already exists with the name '.$data['name']);
            }
        }

        if (!isset($data['permissions']) || !count($data['permissions'])) {
            $data['permissions'] = [];
        }

        //See if the role must contain a permission as per config
        if (config('access.roles.role_must_contain_permission') && count($data['permissions']) === 0) {
            throw new GeneralException(__('exceptions.access.roles.needs_permission'));
        }

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'name' => strtolower($data['name']),
            ])) {
                $module->syncPermissions($data['permissions']);

                event(new RoleUpdated($module));

                return $module;
            }

            throw new GeneralException(trans('exceptions.access.roles.update_error'));
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Role
     * @throws GeneralException
     */
    public function mark($id, $status): Role
    {
        $module = $this->getById($id);

        if ($status === 0 && auth()->id() === $module->id) {
            throw new GeneralException(__('exceptions.access.Users.cant_deactivate_self'));
        }

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new RoleDeactivated($module));
                break;
            case 1:
                event(new RoleReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException(__('exceptions.access.Users.mark_error'));
    }

    /**
     * @param $name
     *
     * @return bool
     */
    protected function roleExists($name): bool
    {
        return $this->model
                ->where('name', strtolower($name))
                ->count() > 0;
    }
}
