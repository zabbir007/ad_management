<?php

namespace App\Repositories;

use App\Events\Segment\SegmentCreated;
use App\Events\Segment\SegmentDeactivated;
use App\Events\Segment\SegmentDeleted;
use App\Events\Segment\SegmentPermanentlyDeleted;
use App\Events\Segment\SegmentReactivated;
use App\Events\Segment\SegmentRestored;
use App\Events\Segment\SegmentUpdated;
use App\Exceptions\GeneralException;
use App\Models\Segment;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Storage;
use Throwable;
use Auth;

/**
 * Class SegmentRepository.
 */
class SegmentRepository extends BaseRepository
{
    /**
     * SegmentRepository constructor.
     *
     * @param  Company  $model
     */
    public function __construct(Segment $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'name',
                'short_name',
                'active',
                'created_at',
                'updated_at'
            ]);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the CompanyGlobalScope trait
        return $dataTableQuery;
    }

    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model
            ->active()
            ->select(['id', 'name'])
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return Segment
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Segment
    {
        return DB::transaction(function () use ($data) {
            $module = $this->model::create([
                'name' => $data['name'],
                'short_name' => $data['short_name'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);
            if ($module) {
                event(new SegmentCreated($module));
                return $module;
            }
            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return Segment
     * @throws Throwable
     */
    public function update($id, array $data): Segment
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'name' => $data['name'],
                'short_name' => $data['short_name'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

                event(new SegmentUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        if ($status === 0 && $module->id === 1) {
            throw new GeneralException('Default Segment can not be deactivated!');
        }

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new SegmentDeactivated($module));
                break;
            case 1:
                event(new SegmentReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new SegmentDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Segment
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Company
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new SegmentPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Segment
     * @throws GeneralException
     */
    public function restore($id): Segment
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new SegmentRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
