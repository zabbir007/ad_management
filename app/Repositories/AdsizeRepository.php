<?php

namespace App\Repositories;

use App\Events\Adsize\AdsizeCreated;
use App\Events\Adsize\AdsizeDeactivated;
use App\Events\Adsize\AdsizeDeleted;
use App\Events\Adsize\AdsizePermanentlyDeleted;
use App\Events\Adsize\AdsizeReactivated;
use App\Events\Adsize\AdsizeRestored;
use App\Events\Adsize\AdsizeUpdated;
use App\Exceptions\GeneralException;
use App\Models\Adsize;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Storage;
use Throwable;
use Auth;

/**
 * Class AdsizeRepository.
 */
class AdsizeRepository extends BaseRepository
{
    /**
     * AdsizeRepository constructor.
     *
     * @param  Adsize  $model
     */
    public function __construct(Adsize $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'size',
                'active',
                'created_at',
                'updated_at'
            ]);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the CompanyGlobalScope trait
        return $dataTableQuery;
    }

    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model
            ->active()
            ->select(['id', 'size'])
            ->get()
            ->pluck('size', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return Adsize
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Adsize
    {
        return DB::transaction(function () use ($data) {
            $module = $this->model::create([
                'size' => $data['size'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);
            if ($module) {
                event(new AdsizeCreated($module));
                return $module;
            }
            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return Adsize
     * @throws Throwable
     */
    public function update($id, array $data): Adsize
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'size' => $data['size'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

                event(new AdsizeUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        if ($status === 0 && $module->id === 1) {
            throw new GeneralException('Default Segment can not be deactivated!');
        }

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new AdsizeDeactivated($module));
                break;
            case 1:
                event(new AdsizeReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module=$this->deleteById($id);
            if ($module) {
                event(new AdsizeDeleted($module));
                return $module;
            }
            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Adsize
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Adsize
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new AdsizePermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Adsize
     * @throws GeneralException
     */
    public function restore($id): Adsize
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new AdsizeRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
