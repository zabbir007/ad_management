<?php

namespace App\Repositories;

use App\Events\Sitesection\SitesectionCreated;
use App\Events\Sitesection\SitesectionDeactivated;
use App\Events\Sitesection\SitesectionDeleted;
use App\Events\Sitesection\SitesectionPermanentlyDeleted;
use App\Events\Sitesection\SitesectionReactivated;
use App\Events\Sitesection\SitesectionRestored;
use App\Events\Sitesection\SitesectionUpdated;
use App\Exceptions\GeneralException;
use App\Models\Sitesection;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Storage;
use Throwable;
use Auth;

/**
 * Class SitesectionRepository.
 */
class SitesectionRepository extends BaseRepository
{
    /**
     * SitesectionRepository constructor.
     *
     * @param  Sitesection  $model
     */
    public function __construct(Sitesection $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'section_name',
                'active',
                'created_at',
                'updated_at'
            ]);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the CompanyGlobalScope trait
        return $dataTableQuery;
    }

    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model
            ->active()
            ->select(['id', 'section_name'])
            ->get()
            ->pluck('section_name', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return Sitesection
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Sitesection
    {
        return DB::transaction(function () use ($data) {
            $module = $this->model::create([
                'section_name' => $data['section_name'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);
            if ($module) {
                event(new SitesectionCreated($module));
                return $module;
            }
            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return Sitesection
     * @throws Throwable
     */
    public function update($id, array $data): Sitesection
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'section_name' => $data['section_name'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

                event(new SitesectionUpdated($module));
                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        if ($status === 0 && $module->id === 1) {
            throw new GeneralException('Default Segment can not be deactivated!');
        }

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new SitesectionDeactivated($module));
                break;
            case 1:
                event(new SitesectionReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module=$this->deleteById($id);
            if ($module) {
                event(new SitesectionDeleted($module));
                return $module;
            }
            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Sitesection
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Sitesection
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new SitesectionPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Sitesection
     * @throws GeneralException
     */
    public function restore($id): Sitesection
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new SitesectionRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
