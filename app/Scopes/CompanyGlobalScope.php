<?php


namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * Class CompanyGlobalScope
 * @package App\Scopes
 */
class CompanyGlobalScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  Builder  $builder
     * @param  Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $id = request()->user()->company_id ?? 0;

        if ($id != 0){
            $builder->where('company_id', '=', $id);
        }
    }
}
