<?php


namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;


/**
 * Class BranchGlobalScope
 * @package App\Scopes
 */
class BranchGlobalScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  Builder  $builder
     * @param  Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $branches = \Session::get('user_branches');

        if (is_array($branches)){
            $branches = \Arr::pluck($branches, 'id');
            $builder->whereIn('branch_id', $branches);
        }
    }
}
