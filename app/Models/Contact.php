<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\ContactAttribute;
use App\Models\Traits\Method\ContactMethod;
use App\Models\Traits\Relationship\ContactRelationship;
use App\Models\Traits\Scope\ContactScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Contact
 *
 * @package App\Models\Master
 */
class Contact extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        ContactAttribute,
        ContactMethod,
        ContactRelationship,
        ContactScope;

    private $module_name = 'Contact';
    private $module_route = 'master.contact';
    private $module_permission = 'setup-contact';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;
    protected $table = 'Contacts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'designation',
        'email',
        'contact_no',
        'active',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
