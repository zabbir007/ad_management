<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\SegmentAttribute;
use App\Models\Traits\Method\SegmentMethod;
use App\Models\Traits\Relationship\SegmentRelationship;
use App\Models\Traits\Scope\SegmentScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Segment
 *
 * @package App\Models\Master
 */
class Segment extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        SegmentAttribute,
        SegmentMethod,
        SegmentRelationship,
        SegmentScope;

    private $module_name = 'Segment';
    private $module_route = 'master.segment';
    private $module_permission = 'setup-segment';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;
    protected $table = 'segments';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'short_name',
        'active',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
