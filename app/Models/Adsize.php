<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\AdsizeAttribute;
use App\Models\Traits\Method\AdsizeMethod;
use App\Models\Traits\Relationship\AdsizeRelationship;
use App\Models\Traits\Scope\AdsizeScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Adsize
 *
 * @package App\Models\Master
 */
class Adsize extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        AdsizeAttribute,
        AdsizeMethod,
        AdsizeRelationship,
        AdsizeScope;

    private $module_name = 'Adsize';
    private $module_route = 'master.adsize';
    private $module_permission = 'setup-adsize';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;
    protected $table = 'ad_sizes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'size',
        'active',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
