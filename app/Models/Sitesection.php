<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\SitesectionAttribute;
use App\Models\Traits\Method\SitesectionMethod;
use App\Models\Traits\Relationship\SitesectionRelationship;
use App\Models\Traits\Scope\SitesectionScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Sitesection
 *
 * @package App\Models\Master
 */
class Sitesection extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        SitesectionAttribute,
        SitesectionMethod,
        SitesectionRelationship,
        SitesectionScope;

    private $module_name = 'Sitesection';
    private $module_route = 'master.sitesection';
    private $module_permission = 'setup-sitesection';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;
    protected $table = 'site_sections';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'section_name',
        'active',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
