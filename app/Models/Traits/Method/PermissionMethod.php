<?php

namespace App\Models\Traits\Method;

/**
 * Trait PermissionMethod.
 */
trait PermissionMethod
{
    /**
     * @return mixed
     */
    public function isDefault()
    {
        return $this->name === 'view backend';
    }


    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
