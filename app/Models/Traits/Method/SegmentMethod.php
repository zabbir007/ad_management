<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait SegmentMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait SegmentMethod
{
    /**
     * @param  bool  $size
     *
     * @return bool|UrlGenerator|mixed|string
     */
    public function getLogo($size = false)
    {
        return asset('img/logo/'.$this->logo);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
