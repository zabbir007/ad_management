<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait SitesectionMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait SitesectionMethod
{
    /**
     * @param  bool  $size
     *
     * @return bool|UrlGenerator|mixed|string
     */
    public function getLogo($size = false)
    {
        return asset('img/logo/'.$this->logo);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
