<?php

namespace App\Models\Traits\Method;

/**
 * Trait RoleMethod.
 */
trait RoleMethod
{
    /**
     * @return mixed
     */
    public function isSuperAdmin()
    {
        return $this->name === config('access.users.superadmin_role');
    }

    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->name === config('access.users.admin_role');
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
