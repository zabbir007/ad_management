<?php

namespace App\Models\Traits\Relationship;

use App\Models\Branch;
use App\Models\Employee;
use App\Models\PasswordHistory;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

    /**
     * @return mixed
     */
    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'user_branches', 'user_id', 'branch_id')->select([
            'branches.id', 'branches.name', 'branches.short_name'
        ]);
    }

    /**
     * @return mixed
     */
    public function employee()
    {
        return $this->hasOne(Employee::class, 'user_id');
    }
}
