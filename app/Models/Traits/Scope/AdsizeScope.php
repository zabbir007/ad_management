<?php

namespace App\Models\Traits\Scope;

/**
 * Trait AdsizeGlobalScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait AdsizeScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
