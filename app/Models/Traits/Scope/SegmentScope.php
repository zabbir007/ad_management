<?php

namespace App\Models\Traits\Scope;

/**
 * Trait SegmentGlobalScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait SegmentScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
