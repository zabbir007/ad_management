<?php

namespace App\Models\Traits\Scope;

/**
 * Trait SitesectionGlobalScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait SitesectionScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
