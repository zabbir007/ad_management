<?php

namespace App\Models\Traits\Scope;

/**
 * Trait PermissionScope
 *
 * @package App\Models\Auth\Traits\Scope
 */
trait PermissionScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
