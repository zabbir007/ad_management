<?php

namespace App\Models\Traits\Scope;

/**
 * Trait CompanyGlobalScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait CompanyScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
