<?php

namespace App\Models\Traits\Scope;

/**
 * Trait ContactGlobalScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait ContactScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
