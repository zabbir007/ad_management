<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\UserAttribute;
use App\Models\Traits\Method\UserMethod;
use App\Models\Traits\Relationship\UserRelationship;
use App\Models\Traits\Scope\UserScope;
use App\Models\Traits\SendUserPasswordReset;
use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Lab404\Impersonate\Models\Impersonate;
use Laravel\Passport\HasApiTokens;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;


/**
 * Class User.
 */
class User extends Authenticatable implements Recordable
{
    use HasRoles,
        Eventually,
        Impersonate,
        HasApiTokens,
        RecordableTrait,
        Notifiable,
        SendUserPasswordReset,
        SoftDeletes,
        Uuid,
        LogsActivity,
        UserAttribute,
        UserMethod,
        UserRelationship,
        UserScope;

    private $module_name = 'User';
    private $module_route = 'user';
    private $module_permission = 'setup-user';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'username',
        'avatar_type',
        'avatar_location',
        'password',
        'password_changed_at',
        'active',
        'confirmation_code',
        'confirmed',
        'timezone',
        'last_login_at',
        'last_login_ip',
        'to_be_logged_out',
    ];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     *
     * @var array
     */
    protected $appends = [
        'full_name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'confirmed' => 'boolean',
        'to_be_logged_out' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'last_login_at',
        'password_changed_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Return true or false if the user can impersonate an other user.
     *
     * @param  void
     * @return  bool
     */
    public function canImpersonate()
    {
        return $this->isSuperAdmin();
    }

    /**
     * Return true or false if the user can be impersonate.
     *
     * @param  void
     * @return  bool
     */
    public function canBeImpersonated()
    {
        return $this->id !== 1;
    }

    /**
     * Find the user instance for the given username.
     *
     * @param $identifier
     * @return User
     */
    public function findForPassport($identifier)
    {
        return $this->where('email', $identifier)->orWhere('username', $identifier)->first();
    }
}
