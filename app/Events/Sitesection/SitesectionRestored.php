<?php

namespace App\Events\Sitesection;

use Illuminate\Queue\SerializesModels;

/**
 * Class SitesectionRestored.
 */
class SitesectionRestored
{
    use SerializesModels;

    /**
     * @var
     */
    public $sitesection;

    /**
     * @param $sitesection
     */
    public function __construct($sitesection)
    {
        $this->sitesection = $sitesection;
    }
}
