<?php

namespace App\Events\Sitesection;

use Illuminate\Queue\SerializesModels;

/**
 * Class SitesectionDeleted.
 */
class SitesectionDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $sitesection;

    /**
     * @param $sitesection
     */
    public function __construct($sitesection)
    {
        $this->sitesection = $sitesection;
    }
}
