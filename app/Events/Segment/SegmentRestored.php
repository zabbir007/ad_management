<?php

namespace App\Events\Segment;

use Illuminate\Queue\SerializesModels;

/**
 * Class SegmentRestored.
 */
class SegmentRestored
{
    use SerializesModels;

    /**
     * @var
     */
    public $segment;

    /**
     * @param $segment
     */
    public function __construct($segment)
    {
        $this->segment = $segment;
    }
}
