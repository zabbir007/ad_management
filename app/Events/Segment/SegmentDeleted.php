<?php

namespace App\Events\Segment;

use Illuminate\Queue\SerializesModels;

/**
 * Class SegmentDeleted.
 */
class SegmentDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $segment;

    /**
     * @param $segment
     */
    public function __construct($segment)
    {
        $this->segment = $segment;
    }
}
