<?php

namespace App\Events\Segment;

use Illuminate\Queue\SerializesModels;

/**
 * Class SegmentCreated.
 */
class SegmentCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $segment;

    /**
     * @param $segment
     */
    public function __construct($segment)
    {
        $this->segment = $segment;
    }
}
