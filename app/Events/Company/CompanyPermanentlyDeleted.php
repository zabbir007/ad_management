<?php

namespace App\Events\Company;

use Illuminate\Queue\SerializesModels;

/**
 * Class CompanyPermanentlyDeleted.
 */
class CompanyPermanentlyDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $company;

    /**
     * @param $company
     */
    public function __construct($company)
    {
        $this->company = $company;
    }
}
