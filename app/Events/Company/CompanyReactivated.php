<?php

namespace App\Events\Company;

use Illuminate\Queue\SerializesModels;

/**
 * Class CompanyReactivated.
 */
class CompanyReactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $company;

    /**
     * @param $company
     */
    public function __construct($company)
    {
        $this->company = $company;
    }
}
