<?php

namespace App\Events\Role;

use Illuminate\Queue\SerializesModels;


/**
 * Class PermissionReactivated
 *
 * @package App\Events\Role
 */
class RoleReactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $module;

    /**
     * @param $module
     */
    public function __construct($module)
    {
        $this->module = $module;
    }
}
