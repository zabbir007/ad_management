<?php

namespace App\Events\Role;

use Illuminate\Queue\SerializesModels;

/**
 * Class PermissionCreated.
 */
class RoleCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $role;

    /**
     * @param $role
     */
    public function __construct($role)
    {
        $this->role = $role;
    }
}
