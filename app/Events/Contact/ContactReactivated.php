<?php

namespace App\Events\Contact;

use Illuminate\Queue\SerializesModels;

/**
 * Class ContactReactivated.
 */
class ContactReactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $contact;

    /**
     * @param $contact
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }
}
