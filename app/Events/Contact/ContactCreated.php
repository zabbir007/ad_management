<?php

namespace App\Events\Contact;

use Illuminate\Queue\SerializesModels;

/**
 * Class SegmentCreated.
 */
class ContactCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $contact;

    /**
     * @param $contact
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }
}
