<?php

namespace App\Events\Contact;

use Illuminate\Queue\SerializesModels;

/**
 * Class ContactDeactivated.
 */
class ContactDeactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $contact;

    /**
     * @param $contact
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }
}
