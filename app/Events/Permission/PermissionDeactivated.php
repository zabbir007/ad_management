<?php

namespace App\Events\Permission;

use Illuminate\Queue\SerializesModels;


/**
 * Class PermissionDeactivated
 *
 * @package App\Events\Permission
 */
class PermissionDeactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $module;

    /**
     * @param $module
     */
    public function __construct($module)
    {
        $this->module = $module;
    }
}
