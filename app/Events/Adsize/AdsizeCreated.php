<?php

namespace App\Events\Adsize;

use Illuminate\Queue\SerializesModels;

/**
 * Class AdsizeCreated.
 */
class AdsizeCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $adsize;

    /**
     * @param $adsize
     */
    public function __construct($adsize)
    {
        $this->adsize = $adsize;
    }
}
