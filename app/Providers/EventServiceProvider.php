<?php

namespace App\Providers;

use App\Listeners\CompanyEventListener;
use App\Listeners\SegmentEventListener;
use App\Listeners\SitesectionEventListener;
use App\Listeners\AdsizeEventListener;
use App\Listeners\GeotargetEventListener;
use App\Listeners\InventorytypeEventListener;
use App\Listeners\ProductEventListener;
use App\Listeners\ContactEventListener;
use App\Listeners\PermissionEventListener;
use App\Listeners\RoleEventListener;
use App\Listeners\UserEventListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Class event subscribers.
     *
     * @var array
     */
    protected $subscribe = [
        UserEventListener::class,
        RoleEventListener::class,
        PermissionEventListener::class,

        //Master
        CompanyEventListener::class,
        SegmentEventListener::class,
        SitesectionEventListener::class,
        AdsizeEventListener::class,
        GeotargetEventListener::class,
        InventorytypeEventListener::class,
        ProductEventListener::class,
        ContactEventListener::class
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
