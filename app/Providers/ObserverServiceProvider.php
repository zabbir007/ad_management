<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Company;
use App\Models\Segment;
use App\Models\Sitesection;
use App\Models\Adsize;
use App\Models\Geotarget;
use App\Models\Inventorytype;
use App\Models\Product;
use App\Models\Contact;

use App\Observers\CompanyObserver;
use App\Observers\SegmentObserver;
use App\Observers\SitesectionObserver;
use App\Observers\AdsizeObserver;
use App\Observers\GeotargetObserver;
use App\Observers\InventorytypeObserver;
use App\Observers\ProductObserver;
use App\Observers\ContactObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

/**
 * Class ObserverServiceProvider.
 */
class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     */
    public function boot()
    {
        User::observe(UserObserver::class);

        //Master Observers
        Company::observe(CompanyObserver::class);
        Segment::observe(SegmentObserver::class);
        Sitesection::observe(SitesectionObserver::class);
        Adsize::observe(AdsizeObserver::class);
        Geotarget::observe(GeotargetObserver::class);
        Inventorytype::observe(InventorytypeObserver::class);
        Product::observe(ProductObserver::class);
        Contact::observe(ContactObserver::class);
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        //
    }
}
