<?php

return [
    'version_name' => env('APP_VERSION_NAME', '1.0'),
    'build' => env('APP_BUILD_VERSION', '1.0.0'),
    'version' => env('APP_BUILD_VERSION', '1.0.0').'.'.app()->version(),
    'js_date_format' => 'mm/dd/yyyy',
    'php_date_format' => 'd-m-Y',
    'date_format' => 'm/d/Y',
    'output_date_format' => 'd M Y',
    'datetime_format' => 'd M Y H:i:s',

    'timeout_status' => env('SESSION_TIMEOUT_STATUS', false),

    'allowed_previous_days' => '3',
    'admin_allowed_previous_days' => '180',

];
