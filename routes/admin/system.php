<?php

/**
 * All route names are prefixed with 'admin.system'.
 */


use Illuminate\Support\Facades\Route;


    /*
    * System Management
    */
    Route::group(['namespace' => 'Activity', 'as' => 'activity_log.', 'prefix' => 'activity_log'], function () {
        Route::get('/', 'ActivityController@index')->name('index');
        Route::get('{activity_log}', 'ActivityController@show')->name('show');
    });

    Route::group(['namespace' => 'TransactionSetting'], function () {
        /*
       * For DataTables
       */
        Route::post('transaction_settings/get', 'TransactionSettingTableController')->name('transaction_settings.get');
        Route::resource('transaction_settings', 'TransactionSettingController');
    });
