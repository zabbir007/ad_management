<?php

use App\Http\Controllers\Company\CompanyController;
use App\Http\Controllers\Company\CompanyStatusController;
use App\Http\Controllers\Company\CompanyTableController;
use App\Http\Controllers\Segment\SegmentController;
use App\Http\Controllers\Segment\SegmentStatusController;
use App\Http\Controllers\Segment\SegmentTableController;
use App\Http\Controllers\Sitesection\SitesectionController;
use App\Http\Controllers\Sitesection\SitesectionStatusController;
use App\Http\Controllers\Sitesection\SitesectionTableController;
use App\Http\Controllers\Adsize\AdsizeController;
use App\Http\Controllers\Adsize\AdsizeStatusController;
use App\Http\Controllers\Adsize\AdsizeTableController;
use App\Http\Controllers\Geotarget\GeotargetController;
use App\Http\Controllers\Geotarget\GeotargetStatusController;
use App\Http\Controllers\Geotarget\GeotargetTableController;
use App\Http\Controllers\Inventorytype\InventorytypeController;
use App\Http\Controllers\Inventorytype\InventorytypeStatusController;
use App\Http\Controllers\Inventorytype\InventorytypeTableController;
use App\Http\Controllers\Product\ProductController;
use App\Http\Controllers\Product\ProductStatusController;
use App\Http\Controllers\Product\ProductTableController;
use App\Http\Controllers\Contact\ContactController;
use App\Http\Controllers\Contact\ContactStatusController;
use App\Http\Controllers\Contact\ContactTableController;
use Illuminate\Support\Facades\Route;

// All route names are prefixed with 'master.'.

Route::group(['prefix' => 'master', 'as' => 'master.'], function () {

    // Company Management
    Route::group(['namespace' => 'Company'], function () {
        // For DataTables
        Route::post('company/get', [CompanyTableController::class, 'datatable'])->name('company.get');

        // Company Status'
        Route::get('company/deactivated',
            [CompanyStatusController::class, 'getDeactivated'])->name('company.deactivated');
        Route::get('company/trashed', [CompanyStatusController::class, 'getDeleted'])->name('company.deleted');

        // Company CRUD
        Route::get('company', [CompanyController::class, 'index'])->name('company.index');
        Route::get('company/create', [CompanyController::class, 'create'])->name('company.create');
        Route::post('company', [CompanyController::class, 'store'])->name('company.store');

        // Specific Company
        Route::group(['prefix' => 'company/{company}'], function () {
            // Company
            Route::get('/', [CompanyController::class, 'show'])->name('company.show');
            Route::get('edit', [CompanyController::class, 'edit'])->name('company.edit');
            Route::patch('/', [CompanyController::class, 'update'])->name('company.update');
            Route::delete('/', [CompanyController::class, 'destroy'])->name('company.destroy');

            // Status
            Route::get('mark/{status}',
                [CompanyStatusController::class, 'mark'])->name('company.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [CompanyStatusController::class, 'delete'])->name('company.delete-permanently');
            Route::get('restore', [CompanyStatusController::class, 'restore'])->name('company.restore');
        });
    });


    // Segment Management
    Route::group(['namespace' => 'Segment'], function () {
        // For DataTables
        Route::post('segment/get', [SegmentTableController::class, 'datatable'])->name('segment.get');

        // segment Status'
        Route::get('segment/deactivated',
            [SegmentStatusController::class, 'getDeactivated'])->name('segment.deactivated');
        Route::get('segment/trashed', [SegmentStatusController::class, 'getDeleted'])->name('segment.deleted');

        // Segment CRUD
        Route::get('segment', [SegmentController::class, 'index'])->name('segment.index');
        Route::get('segment/create', [SegmentController::class, 'create'])->name('segment.create');
        Route::post('segment', [SegmentController::class, 'store'])->name('segment.store');

        // Specific Segment
        Route::group(['prefix' => 'segment/{segment}'], function () {
            // Segment
            Route::get('/', [SegmentController::class, 'show'])->name('segment.show');
            Route::get('edit', [SegmentController::class, 'edit'])->name('segment.edit');
            Route::patch('/', [SegmentController::class, 'update'])->name('segment.update');
            Route::delete('/', [SegmentController::class, 'destroy'])->name('segment.destroy');

            // Status
            Route::get('mark/{status}',
                [SegmentStatusController::class, 'mark'])->name('segment.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [SegmentStatusController::class, 'delete'])->name('segment.delete-permanently');
            Route::get('restore', [SegmentStatusController::class, 'restore'])->name('segment.restore');
        });
    });

    // Contract Management
    Route::group(['namespace' => 'Contact'], function () {
        // For DataTables
        Route::post('contact/get', [ContactTableController::class, 'datatable'])->name('contact.get');

        // Contact Status'
        Route::get('contact/deactivated',
            [ContactStatusController::class, 'getDeactivated'])->name('contact.deactivated');
        Route::get('contact/trashed', [ContactStatusController::class, 'getDeleted'])->name('contact.deleted');

        // contract CRUD
        Route::get('contact', [ContactController::class, 'index'])->name('contact.index');
        Route::get('contact/create', [ContactController::class, 'create'])->name('contact.create');
        Route::post('contact', [ContactController::class, 'store'])->name('contact.store');

        // Specific Contact
        Route::group(['prefix' => 'contact/{contact}'], function () {
            // Contact
            Route::get('/', [ContactController::class, 'show'])->name('contact.show');
            Route::get('edit', [ContactController::class, 'edit'])->name('contact.edit');
            Route::patch('/', [ContactController::class, 'update'])->name('contact.update');
            Route::delete('/', [ContactController::class, 'destroy'])->name('contact.destroy');

            // Status
            Route::get('mark/{status}',
                [ContactStatusController::class, 'mark'])->name('contact.mark')->where(['status' => '[0,1]']);
            // Deleted
            Route::get('delete', [ContactStatusController::class, 'delete'])->name('contact.delete-permanently');
            Route::get('restore', [ContactStatusController::class, 'restore'])->name('contact.restore');
        });
    });

    // Ad Size Management
    Route::group(['namespace' => 'Adsize'], function () {
        // For DataTables
        Route::post('adsize/get', [AdsizeTableController::class, 'datatable'])->name('adsize.get');

        // Adsize Status'
        Route::get('adsize/deactivated',
            [AdsizeStatusController::class, 'getDeactivated'])->name('adsize.deactivated');
        Route::get('adsize/trashed', [AdsizeStatusController::class, 'getDeleted'])->name('adsize.deleted');

        // Adsize CRUD
        Route::get('adsize', [AdsizeController::class, 'index'])->name('adsize.index');
        Route::get('adsize/create', [AdsizeController::class, 'create'])->name('adsize.create');
        Route::post('adsize', [AdsizeController::class, 'store'])->name('adsize.store');

        // Specific Adsize
        Route::group(['prefix' => 'adsize/{adsize}'], function () {
            // Contact
            Route::get('/', [AdsizeController::class, 'show'])->name('adsize.show');
            Route::get('edit', [AdsizeController::class, 'edit'])->name('adsize.edit');
            Route::patch('/', [AdsizeController::class, 'update'])->name('adsize.update');
            Route::delete('/', [AdsizeController::class, 'destroy'])->name('adsize.destroy');

            // Status
            Route::get('mark/{status}',
                [AdsizeStatusController::class, 'mark'])->name('adsize.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [AdsizeStatusController::class, 'delete'])->name('adsize.delete-permanently');
            Route::get('restore', [AdsizeStatusController::class, 'restore'])->name('adsize.restore');
        });
    });

    // Site Section Management
    Route::group(['namespace' => 'SiteSection'], function () {
        // For DataTables
        Route::post('sitesection/get', [SitesectionTableController::class, 'datatable'])->name('sitesection.get');

        // Adsize Status'
        Route::get('sitesection/deactivated',
            [SitesectionStatusController::class, 'getDeactivated'])->name('sitesection.deactivated');
        Route::get('sitesection/trashed', [SitesectionStatusController::class, 'getDeleted'])->name('sitesection.deleted');

        // sitesection CRUD
        Route::get('sitesection', [SitesectionController::class, 'index'])->name('sitesection.index');
        Route::get('sitesection/create', [SitesectionController::class, 'create'])->name('sitesection.create');
        Route::post('sitesection', [SitesectionController::class, 'store'])->name('sitesection.store');

        // Specific sitesection
        Route::group(['prefix' => 'sitesection/{sitesection}'], function () {
            // sitesection
            Route::get('/', [SitesectionController::class, 'show'])->name('sitesection.show');
            Route::get('edit', [SitesectionController::class, 'edit'])->name('sitesection.edit');
            Route::patch('/', [SitesectionController::class, 'update'])->name('sitesection.update');
            Route::delete('/', [SitesectionController::class, 'destroy'])->name('sitesection.destroy');

            // Status
            Route::get('mark/{status}',
                [SitesectionStatusController::class, 'mark'])->name('sitesection.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [SitesectionStatusController::class, 'delete'])->name('sitesection.delete-permanently');
            Route::get('restore', [SitesectionStatusController::class, 'restore'])->name('sitesection.restore');
        });
    });

    // Product Management
    Route::group(['namespace' => 'Product'], function () {
        // For DataTables
        Route::post('product/get', [ProductTableController::class, 'datatable'])->name('product.get');

        // Product Status'
        Route::get('product/deactivated',
            [ProductStatusController::class, 'getDeactivated'])->name('product.deactivated');
        Route::get('product/trashed', [ProductStatusController::class, 'getDeleted'])->name('product.deleted');

        // Product CRUD
        Route::get('product', [ProductController::class, 'index'])->name('product.index');
        Route::get('product/create', [ProductController::class, 'create'])->name('product.create');
        Route::post('product', [ProductController::class, 'store'])->name('product.store');

        // Specific Product
        Route::group(['prefix' => 'product/{product}'], function () {
            // Product
            Route::get('/', [ProductController::class, 'show'])->name('product.show');
            Route::get('edit', [ProductController::class, 'edit'])->name('product.edit');
            Route::patch('/', [ProductController::class, 'update'])->name('product.update');
            Route::delete('/', [ProductController::class, 'destroy'])->name('product.destroy');

            // Status
            Route::get('mark/{status}',
                [ProductStatusController::class, 'mark'])->name('product.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [ProductStatusController::class, 'delete'])->name('product.delete-permanently');
            Route::get('restore', [ProductStatusController::class, 'restore'])->name('product.restore');
        });
    });


    // Inventory Type Management
    Route::group(['namespace' => 'Inventorytype'], function () {
        // For DataTables
        Route::post('inventorytype/get', [InventorytypeTableController::class, 'datatable'])->name('inventorytype.get');

        // Inventorytype Status'
        Route::get('inventorytype/deactivated',
            [InventorytypeStatusController::class, 'getDeactivated'])->name('inventorytype.deactivated');
        Route::get('inventorytype/trashed', [InventorytypeStatusController::class, 'getDeleted'])->name('inventorytype.deleted');

        // Inventorytype CRUD
        Route::get('inventorytype', [InventorytypeController::class, 'index'])->name('inventorytype.index');
        Route::get('inventorytype/create', [InventorytypeController::class, 'create'])->name('inventorytype.create');
        Route::post('inventorytype', [InventorytypeController::class, 'store'])->name('inventorytype.store');

        // Specific Inventorytype
        Route::group(['prefix' => 'inventorytype/{inventorytype}'], function () {
            // Inventorytype
            Route::get('/', [InventorytypeController::class, 'show'])->name('inventorytype.show');
            Route::get('edit', [InventorytypeController::class, 'edit'])->name('inventorytype.edit');
            Route::patch('/', [InventorytypeController::class, 'update'])->name('inventorytype.update');
            Route::delete('/', [InventorytypeController::class, 'destroy'])->name('inventorytype.destroy');

            // Status
            Route::get('mark/{status}',
                [InventorytypeStatusController::class, 'mark'])->name('inventorytype.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [InventorytypeStatusController::class, 'delete'])->name('inventorytype.delete-permanently');
            Route::get('restore', [InventorytypeStatusController::class, 'restore'])->name('inventorytype.restore');
        });
    });

    // Geo Target Management
    Route::group(['namespace' => 'Geotarget'], function () {
        // For DataTables
        Route::post('geotarget/get', [GeotargetTableController::class, 'datatable'])->name('geotarget.get');

        // geotarget Status'
        Route::get('geotarget/deactivated',
            [GeotargetStatusController::class, 'getDeactivated'])->name('geotarget.deactivated');
        Route::get('geotarget/trashed', [GeotargetStatusController::class, 'getDeleted'])->name('geotarget.deleted');

        // geotarget CRUD
        Route::get('geotarget', [GeotargetController::class, 'index'])->name('geotarget.index');
        Route::get('geotarget/create', [GeotargetController::class, 'create'])->name('geotarget.create');
        Route::post('geotarget', [GeotargetController::class, 'store'])->name('geotarget.store');

        // Specific geotarget
        Route::group(['prefix' => 'geotarget/{geotarget}'], function () {
            // Inventorytype
            Route::get('/', [GeotargetController::class, 'show'])->name('geotarget.show');
            Route::get('edit', [GeotargetController::class, 'edit'])->name('geotarget.edit');
            Route::patch('/', [GeotargetController::class, 'update'])->name('geotarget.update');
            Route::delete('/', [GeotargetController::class, 'destroy'])->name('geotarget.destroy');

            // Status
            Route::get('mark/{status}',
                [GeotargetStatusController::class, 'mark'])->name('geotarget.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [GeotargetStatusController::class, 'delete'])->name('geotarget.delete-permanently');
            Route::get('restore', [GeotargetStatusController::class, 'restore'])->name('geotarget.restore');
        });
    });

});
