<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\LanguageController;
use Illuminate\Support\Facades\Route;

/*
 * Global Routes
 */

// Switch between the included languages
Route::get('lang/{lang}', [LanguageController::class, 'swap']);

Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {
    // Authentication Routes
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('index');

    Route::group(['middleware' => 'guest'], function () {
        Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
        Route::post('login', [LoginController::class, 'login'])->name('login.post');

//        // Password Reset Routes
//        Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.email');
//        Route::post('password/email',
//            [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email.post');
//
//        Route::get('password/reset/{token}',
//            [ResetPasswordController::class, 'showResetForm'])->name('password.reset.form');
//        Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.reset');
    });
});

/*
 * Routes
 * Namespaces indicate folder structure
 */
Route::group(['middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the admin,
     * then limit the admin features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     * These routes can not be hit if the password is expired
     */
    include_route_files(__DIR__.'/admin/');
});
