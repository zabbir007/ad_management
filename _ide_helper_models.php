<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * Class Branch
 *
 * @package App\Models\Master
 * @property int $id
 * @property string|null $code
 * @property string $name
 * @property string $short_name
 * @property string|null $address1
 * @property string|null $address2
 * @property string|null $address3
 * @property string|null $address4
 * @property string|null $email
 * @property string|null $location
 * @property int|null $bic_id
 * @property int|null $dbic_id
 * @property int|null $aic_id
 * @property string $order
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereAddress3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereAddress4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereAicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereBicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereDbicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch withoutTrashed()
 */
	class Branch extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class Company
 *
 * @package App\Models\Master
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $short_name
 * @property string|null $address
 * @property string|null $logo
 * @property string|null $email_suffix
 * @property string $time_zone
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read string $actions
 * @property-read mixed $company_logo
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereEmailSuffix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereTimeZone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company withoutTrashed()
 */
	class Company extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class Designation
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $company_id
 * @property string|null $name
 * @property string|null $order
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Designation onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Designation whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Designation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Designation withoutTrashed()
 */
	class Designation extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class DocumentType
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $company_id
 * @property int $type 1=driver, 2=vehicle
 * @property string $name
 * @property int $order
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Driver $driver
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType driver()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType vehicle()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentType whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType withoutTrashed()
 */
	class DocumentType extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class Driver
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $company_id
 * @property string|null $employee_id driver payroll id
 * @property string|null $name
 * @property string|null $mobile
 * @property string|null $dob
 * @property string|null $doj
 * @property string|null $blood_group
 * @property string|null $present_address
 * @property string|null $permanent_address
 * @property string|null $em_mobile emergency mobile number
 * @property string|null $photo
 * @property int $order
 * @property string|null $custom1
 * @property string|null $custom2
 * @property string|null $custom3
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch[] $branch
 * @property-read int|null $branch_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DriverDocument[] $documents
 * @property-read int|null $documents_count
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read mixed|string|null $first_name
 * @property-read mixed|string|null $last_name
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\VehicleDriver|null $vehicle
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Driver onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereBloodGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereCustom1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereCustom2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereCustom3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereDoj($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereEmMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver wherePermanentAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver wherePresentAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Driver withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Driver withoutTrashed()
 */
	class Driver extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class DriverDocument
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $driver_id
 * @property int $document_type 1 = NID card, 2=license
 * @property string|null $document_no
 * @property string|null $issue_date
 * @property string|null $expiry_date
 * @property string|null $issuing_authority
 * @property int|null $order
 * @property string|null $custom1
 * @property string|null $custom2
 * @property string|null $custom3
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Driver $driver
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DriverDocument onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereCustom1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereCustom2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereCustom3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereDocumentNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereDriverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereExpiryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereIssueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereIssuingAuthority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DriverDocument whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DriverDocument withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DriverDocument withoutTrashed()
 */
	class DriverDocument extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class Employee
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $company_id
 * @property int|null $user_id
 * @property string|null $employee_id
 * @property string|null $name
 * @property int $designation_id
 * @property string|null $grade
 * @property string|null $mobile_no
 * @property string|null $email
 * @property string|null $department
 * @property string|null $order
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Designation $designation
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read mixed|string|null $first_name
 * @property-read mixed|string|null $last_name
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee designation($designation)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereDesignationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereMobileNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee withoutTrashed()
 */
	class Employee extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class Fuel
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string|null $short_name
 * @property int $order
 * @property string|null $custom1
 * @property string|null $custom2
 * @property string|null $custom3
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FuelRate[] $branch
 * @property-read int|null $branch_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Fuel[] $fuel
 * @property-read int|null $fuel_count
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FuelRate[] $rates
 * @property-read int|null $rates_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Fuel onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereCustom1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereCustom2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereCustom3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fuel whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Fuel withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Fuel withoutTrashed()
 */
	class Fuel extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class FuelRate
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $company_id
 * @property \Illuminate\Support\Carbon $entry_date
 * @property int $fuel_id
 * @property int $branch_id
 * @property float $rate
 * @property int $order
 * @property int $archived
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property mixed|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Branch $Branch
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Company $company
 * @property-read \App\Models\Fuel $fuel
 * @property-read string $actions
 * @property-read string $edit_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate archived($status = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FuelRate onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereArchived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereEntryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereFuelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FuelRate whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FuelRate withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FuelRate withoutTrashed()
 */
	class FuelRate extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class LogbookEntry
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $company_id
 * @property string $sl transaction no
 * @property string $trans_date
 * @property int $branch_id
 * @property string $branch_name
 * @property int $employee_id NDM id
 * @property string $employee_name
 * @property int $vehicle_id
 * @property string|null $vehicle_name
 * @property string $vehicle_license_no
 * @property int $vehicle_mnf_yr vehicle manufacturing year
 * @property int $vehicle_purchase_yr vehicle purchase year
 * @property int $vehicle_lifetime vehicle lifetime till date
 * @property int $vehicle_used vehicle used (in days) as per action plan
 * @property int $driver_id
 * @property string $driver_name
 * @property string $driver_take_ovr_dt vehicle key take over date
 * @property string $driver_hand_ovr_dt vehicle key hand over date
 * @property float $rkm_data RKM VTS Data (KM)
 * @property float $logbook_opening KM of Logbook opening
 * @property float $logbook_closing KM of Logbook closing
 * @property float $logbook_running KM of total running
 * @property int $fuel_id
 * @property string $fuel_name
 * @property float $fuel_consumption
 * @property float $fuel_rate
 * @property float $fuel_cost
 * @property float $std_fuel_consumption standard_fuel_consumption
 * @property int $approved
 * @property int|null $approved_by
 * @property int $status
 * @property int|null $order
 * @property int $year
 * @property int $month
 * @property string $month_name
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property mixed|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Branch $Branch
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Company $company
 * @property-read \App\Models\Fuel $fuel
 * @property-read string $actions
 * @property-read string $approve_button
 * @property-read string $approve_label
 * @property-read string $edit_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry active($status = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LogbookEntry onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereApprovedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereBranchName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereDriverHandOvrDt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereDriverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereDriverName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereDriverTakeOvrDt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereEmployeeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereFuelConsumption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereFuelCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereFuelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereFuelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereFuelRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereLogbookClosing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereLogbookOpening($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereLogbookRunning($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereMonthName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereRkmData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereSl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereStdFuelConsumption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereTransDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereVehicleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereVehicleLicenseNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereVehicleLifetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereVehicleMnfYr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereVehicleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereVehiclePurchaseYr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereVehicleUsed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogbookEntry whereYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LogbookEntry withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LogbookEntry withoutTrashed()
 */
	class LogbookEntry extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class PasswordHistory.
 *
 * @property int $id
 * @property int $user_id
 * @property string $password
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PasswordHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PasswordHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PasswordHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PasswordHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PasswordHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PasswordHistory wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PasswordHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PasswordHistory whereUserId($value)
 */
	class PasswordHistory extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Permission.
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read string $actions
 * @property-read string $edit_button
 * @property-read string $show_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Permission permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Permission role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class Role.
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class User.
 *
 * @property int $id
 * @property int $company_id
 * @property string|null $first_name
 * @property string $last_name
 * @property string $email
 * @property string $username
 * @property string|null $email_verified_at
 * @property string $avatar_type
 * @property string|null $avatar_location
 * @property string $password
 * @property \Illuminate\Support\Carbon|null $password_changed_at
 * @property bool $active
 * @property string|null $confirmation_code
 * @property bool $confirmed
 * @property string|null $timezone
 * @property int $is_logged
 * @property \Illuminate\Support\Carbon|null $last_login_at
 * @property string|null $last_login_ip
 * @property bool $to_be_logged_out
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch[] $branches
 * @property-read int|null $branches_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read string $actions
 * @property-read string $branch_actions
 * @property-read string $change_password_button
 * @property-read string $clear_session_button
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_branch_button
 * @property-read string $edit_button
 * @property-read string $full_name
 * @property-read string $login_as_button
 * @property-read string $name
 * @property-read string $permissions_label
 * @property-read mixed $picture
 * @property-read string $restore_button
 * @property-read string $roles_label
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PasswordHistory[] $passwordHistories
 * @property-read int|null $password_histories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User confirmed($confirmed = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User uuid($uuid)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAvatarLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAvatarType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereConfirmationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereIsLogged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePasswordChangedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereToBeLoggedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 */
	class User extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class UserBranch
 *
 * @package App\Models
 * @property int $id
 * @property int $user_id
 * @property int $branch_id
 * @property-read \App\Models\Branch $branch
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBranch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBranch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBranch query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBranch whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBranch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBranch whereUserId($value)
 */
	class UserBranch extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Vehicle
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $company_id
 * @property int $vehicle_type
 * @property string $name
 * @property string $manufacturer
 * @property string|null $model
 * @property int $manufacturer_year
 * @property string|null $weight
 * @property string|null $lifetime
 * @property string $license_plate_no
 * @property int $license_year
 * @property bool $is_dual_tank
 * @property int $main_tank_fuel_id
 * @property int $main_tank_fuel_capacity
 * @property int|null $second_tank_fuel_id
 * @property int|null $second_tank_fuel_capacity
 * @property string $chassis_no
 * @property string $engine_no
 * @property string|null $vin_no Vehicle identification no
 * @property float $opening
 * @property string|null $purchase_date
 * @property int $order
 * @property string|null $custom1
 * @property string|null $custom2
 * @property string|null $custom3
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch[] $branches
 * @property-read int|null $branches_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VehicleDocument[] $documents
 * @property-read int|null $documents_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Driver[] $drivers
 * @property-read int|null $drivers_count
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $format_name
 * @property-read string $purchase_year
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VehicleDriver[] $histories
 * @property-read int|null $histories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \App\Models\Fuel $main_fuel
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Fuel|null $secondary_fuel
 * @property-read \App\Models\VehicleType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vehicle onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereChassisNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCustom1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCustom2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCustom3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereEngineNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereIsDualTank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereLicensePlateNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereLicenseYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereLifetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereMainTankFuelCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereMainTankFuelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereManufacturer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereManufacturerYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereOpening($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle wherePurchaseDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereSecondTankFuelCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereSecondTankFuelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereVehicleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereVinNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vehicle withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vehicle withoutTrashed()
 */
	class Vehicle extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class VehicleDocument
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $vehicle_id
 * @property int $document_type
 * @property string|null $document_no
 * @property string|null $issue_date
 * @property string|null $expiry_date
 * @property string|null $issuing_authority
 * @property int|null $order
 * @property string|null $custom1
 * @property string|null $custom2
 * @property string|null $custom3
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleDocument onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereCustom1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereCustom2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereCustom3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereDocumentNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereExpiryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereIssueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereIssuingAuthority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDocument whereVehicleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleDocument withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleDocument withoutTrashed()
 */
	class VehicleDocument extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class VehicleDriver
 *
 * @package App\Models
 * @property int $id
 * @property int $company_id
 * @property int $vehicle_id
 * @property int $branch_id
 * @property int $driver_id
 * @property string $take_over
 * @property string|null $hand_over
 * @property int $order
 * @property string|null $custom1
 * @property string|null $custom2
 * @property string|null $custom3
 * @property bool $archived
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Branch $branch
 * @property-read \App\Models\Driver $driver
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Vehicle $vehicle
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver archived($status = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleDriver onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereArchived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereCustom1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereCustom2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereCustom3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereDriverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereHandOver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereTakeOver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleDriver whereVehicleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleDriver withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleDriver withoutTrashed()
 */
	class VehicleDriver extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class VehicleHistory
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $company_id
 * @property string $entry_date
 * @property int $vehicle_id
 * @property int $fuel_id
 * @property int $tanks number of fuel tanks
 * @property int $tank_type 1=main or 2=secondary tank
 * @property float $capacity fuel capacity
 * @property float $millage standard millage
 * @property int $order
 * @property string|null $custom1
 * @property string|null $custom2
 * @property string|null $custom3
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $format_name
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Vehicle $vehicle
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleHistory onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereCustom1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereCustom2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereCustom3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereEntryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereFuelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereMillage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereTankType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereTanks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleHistory whereVehicleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleHistory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleHistory withoutTrashed()
 */
	class VehicleHistory extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

namespace App\Models{
/**
 * Class VehicleType
 *
 * @package App\Models\Master
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property int $order
 * @property bool $active
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read string $actions
 * @property-read string $delete_button
 * @property-read string $delete_permanently_button
 * @property-read string $edit_button
 * @property-read string $restore_button
 * @property-read string $show_button
 * @property-read string $status_button
 * @property-read string $status_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Altek\Accountant\Models\Ledger[] $ledgers
 * @property-read int|null $ledgers_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType active($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleType onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VehicleType whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VehicleType withoutTrashed()
 */
	class VehicleType extends \Eloquent implements \Altek\Accountant\Contracts\Recordable, \Altek\Accountant\Contracts\Identifiable {}
}

