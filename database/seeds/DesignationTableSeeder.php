<?php

use Illuminate\Database\Seeder;

class DesignationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee_designations = array(
            array('id' => '1','company_id' => '1','name' => 'NDM','order' => '1','active' => '1','created_by' => '1','updated_by' => NULL,'deleted_by' => NULL,'created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL)
        );

        DB::table('employee_designations')->insert($employee_designations);
    }
}
