<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        $superadmin = Role::create(['name' => config('access.users.superadmin_role')]);
        $admin = Role::create(['name' => config('access.users.admin_role')]);
        $user = Role::create(['name' => config('access.users.default_role')]);;

        // Create Permissions
        $permissions = ['view backend'];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

//        // ALWAYS GIVE SUPER ADMIN ROLE ALL PERMISSIONS
//        $superadmin->givePermissionTo(Permission::all());

        $admin->givePermissionTo(Permission::findById(1)->name);

        $user->givePermissionTo(Permission::findById(1)->name);


        // Assign Permissions to other Roles
        // Note: Admin (User 1) Has all permissions via a gate in the AuthServiceProvider
        // $user->givePermissionTo('view backend');

        $this->enableForeignKeys();
    }
}
