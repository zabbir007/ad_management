<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'roles' => [
        'created' => 'Rol uğurla yaradıldı.',
        'deleted' => 'Rol uğurla silindi.',
        'updated' => 'Rol uğurla yeniləndi.',
    ],

    'users' => [
        'cant_resend_confirmation' => 'Tətbiq hazırda istifadəçiləri əl ilə təsdiqləmək üçün ayarlanıb.',
        'confirmation_email' => 'Yeni təsdiqləmə emaili fayldakı ünvana göndərildi.',
        'confirmed' => 'İstifadəçi uğurla təsdiqləndi.',
        'created' => 'İstifadəçi uğurla yaradıldı.',
        'deleted' => 'İstifadəçi uğurla silindi.',
        'deleted_permanently' => 'İstifadəçi davamlı olaraq silindi.',
        'restored' => 'İstifadəçi uğurla geriyə yükləndi.',
        'session_cleared' => ' İstifadəçinin sessiyası uğurla təmizləndi.',
        'unconfirmed' => 'İstifadəçi təsdiqlənmədi',
        'updated' => 'İstifadəçi uğurla yeniləndi.',
        'updated_password' => ' İstifadəçinin şifrəsi uğurla yeniləndi.',
    ],

    'contact' => [
        'sent' => 'Sizin məlumatınız uğurla göndərildi. Qısa zaman içində əks-əlaqə ediləcəkdir.',
    ],

];
