require('datatables.net');
require('datatables.net-bs4');
require('datatables.net-buttons');
require('datatables.net-buttons-bs4');
require('datatables.net-buttons/js/buttons.print');
require('datatables.net-buttons/js/buttons.html5');
require('datatables.net-buttons/js/buttons.flash');
require('datatables.net-buttons/js/buttons.colVis');
require('datatables.net-fixedheader-bs4');
require('datatables.net-autofill-bs4');
require('datatables.net-select-bs4');
require('datatables.net-responsive-bs4');
require('select2');
require('bootstrap-datepicker');
require('chart.js/dist/Chart.bundle.min');


window.toastr = require('toastr');

window.dataTableOptions = {
    initComplete: (settings, json) => {
        addDeleteForms();
    },
    // pagingType: "simple_numbers",
    // // stateSave: true,
    // // autoWidth: 1,
    // responsive: false,
    pageLength: 10,
    lengthMenu: [[10, 25, 35, 50, -1], [10, 25, 35, 50, "All"]],
    // fixedHeader: false,
    // sWrapper: "dataTables_wrapper dt-bootstrap4",
    // sFilterInput: "form-control form-control-sm",
    // sLengthSelect: "form-control form-control-sm",
    // language: {
    //     searchPlaceholder: "Search..",
    //     paginate: {
    //         first: '<i class="fa fa-angle-double-left"></i>',
    //         previous: '<i class="fa fa-angle-left"></i>',
    //         next: '<i class="fa fa-angle-right"></i>',
    //         last: '<i class="fa fa-angle-double-right"></i>'
    //     },
    //     loadingRecords: '&nbsp;',
    //     processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>Loading...',
    //     aria: {
    //         sortAscending: ": activate to sort column ascending",
    //         sortDescending: ": activate to sort column descending"
    //     },
    //     emptyTable: "No data available in table",
    //     info: "Showing _START_ to _END_ of _TOTAL_ entries",
    //     infoEmpty: "No entries found",
    //     infoFiltered: "(filtered1 from _MAX_ total entries)",
    //     lengthMenu: "_MENU_ entries",
    //     search: "Search:",
    //     zeroRecords: "No matching records found"
    // },
    // dom: "<'row'<'col-sm-12 col-md-3 text-left'f>" +
    //     "<'col-sm-12 col-md-4'>" +
    //     // "<'col-sm-12 col-md-4'B>" +
    //     "<'col-sm-12 col-md-2'l>" +
    //     "<'col-sm-12 col-md-3 text-right'pi>>" +
    //     "<'row'<'col-sm-12'tr>>",
    // buttons: [
    //     'colvis',
    //     'copyHtml5',
    //     'csvHtml5',
    //     'excelHtml5',
    //     'pdfHtml5',
    //     'print'
    // ]
};
