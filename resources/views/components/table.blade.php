<div class="table-responsive">
    <table class="{{ $table_class ?? 'table table-sm table-condensed table-bordered table-striped table-vcenter' }}"
           id="{{ $table_id ?? 'datatable' }}">
        <thead class="{{ $thead_class ?? '' }}" id="{{ $thead_id ?? '' }}">
        {{ $thead_content ?? '' }}
        </thead>
        <tbody class="{{ $tbody_class ?? '' }}" id="{{ $tbody_id ?? '' }}">
        {{ $tbody_content ?? '' }}
        </tbody>
        <tfoot class="{{ $tfoot_class ?? '' }}" id="{{ $tfoot_id ?? '' }}">
        {{ $tfoot_content ?? '' }}
        </tfoot>
    </table>
</div>
