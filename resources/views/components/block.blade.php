<div class="card card-outline card-primary">
    @if(isset($main_heading) || isset($sub_heading))
        <div class="card-header p-1">
            @isset($main_heading)
                <h2 class="card-title pt-1 pl-1">
                   <i class="{{ $icon ?? '' }}"></i>&nbsp;{{ $main_heading }}
                    @isset($sub_heading)
                        <small class="text-muted">{{ $sub_heading }}</small>
                    @endisset
                </h2>
            @endisset

            @isset($options)
                <div class="card-tools mr-1">
                    {{ $options }}
                </div>
            @endisset
        </div>
    @endif
    <div class="card-body p-2">
        @isset($content)
            {{ $content }}
        @endisset
    </div>
    <!-- /.card-body -->

    @isset($footer)
        <div class="card-footer mb-0 pl-3">
            {{ $footer ?? '' }}
        </div>
    @endisset
</div>
<!-- /.card -->
