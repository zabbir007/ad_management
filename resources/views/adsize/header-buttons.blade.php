<div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
    <div class="btn-group btn-group-sm">
        @can($module_permission.'-edit')
            @if(request()->routeIs('*.show'))
                <a href="{{ route($module_route.'.edit', $module) }}"
                   class="btn bg-primary btn-flat btn-outline-primary bg-gradient-primary mr-1"><i
                            class="fas fa-pen"></i>&nbsp;Update {{ ucfirst($module_name) }}</a>
            @endif
        @endcan

        @can($module_permission.'-show')
            @if(request()->routeIs('*.edit'))
                <a href="{{ route($module_route.'.show', $module) }}"
                   class="btn bg-primary btn-flat btn-outline-primary bg-gradient-primary mr-1"><i
                            class="fas fa-eye"></i>&nbsp;View {{ ucfirst($module_name) }}</a>
            @endif
        @endcan

        @can($module_permission.'-create')
            <a href="{{ route($module_route.'.create') }}"
               class="btn bg-success btn-flat btn-outline-success bg-gradient-success mr-1"><i
                        class="fas fa-plus-circle"></i>&nbsp;Create {{ ucfirst($module_name) }}</a>
        @endcan

        @can($module_permission.'-index')
            <a href="{{ route($module_route.'.index') }}"
               class="btn bg-success btn-flat btn-outline-success bg-gradient-success mr-1">&nbsp;<i
                        class="fas fa-th-list"></i>&nbsp;All {{ str_plural(ucfirst($module_name)) }}</a>
        @endcan

        <div class="dropdown">
            <a class="btn btn-default btn-flat btn-outline-primary dropdown-toggle dropdown-icon" type="button"
               id="dropdownMenu1" data-toggle="dropdown">
                <i class="fas fa-cogs"></i>&nbsp;More
            </a>
            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                @can($module_permission.'-view-deactivated')
                    <li role="presentation"><a class="dropdown-item" role="menuitem" tabindex="-1"
                                               href="{{ route($module_route.'.deactivated') }}"><i
                                    class="fas fa-ban"></i>&nbsp;Deactivated {{ str_plural(ucfirst($module_name)) }}
                        </a>
                    </li>
                @endcan
                @can($module_permission.'-view-deleted')
                    <li role="presentation"><a class="dropdown-item" role="menuitem" tabindex="-1"
                                               href="{{ route($module_route.'.deleted') }}"><i
                                    class="fas fa-trash"></i>&nbsp;Trashed {{ str_plural(ucfirst($module_name)) }}
                        </a>
                    </li>
                @endcan
            </ul>
        </div>
    </div>
</div>