@extends('layouts.app')

@section('title', app_name() . ' | ' . __('strings.dashboard.title'))

@section('content')
    <!-- Hero -->
    <div class="bg-image overflow-hidden" style="background-image: url({{ asset('media/photos/photo3@2x.jpg') }});">
        <div class="bg-primary-dark-op">
            <div class="content content-narrow content-full">
                <div
                    class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center my-3 text-center text-sm-left">
                    <div class="flex-sm-fill">
                        <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Dashboard</h1>
                        <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear"
                            data-timeout="250">@lang('strings.dashboard.welcome') {{ $logged_in_user->name }}</h2>
                    </div>
                    <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                            <span class="d-inline-block invisible" data-toggle="appear" data-timeout="350">
                                <a class="btn btn-primary px-4 py-2" data-toggle="click-ripple"
                                   href="">
                                    <i class="fa fa-plus mr-1"></i> New Request
                                </a>
                            </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->

@endsection

@push('after-scripts')
    <script>
        // jQuery(function () {
        //     One.helpers(['chart', 'easy-pie-chart', 'sparkline']);
        // });
    </script>
@endpush
