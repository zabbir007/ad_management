<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Odometer')">
    <meta name="author" content="@yield('meta_author', 'Md. Aftab Uddin')">
@yield('meta')

{{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
@stack('before-styles')

<!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    {{ style(mix('css/app.css')) }}
    <style>
        body {
            background: url({{URL::asset('/img/login_bg.png')}}) no-repeat center center fixed;
            /*-webkit-background-size: cover;*/
            /*-moz-background-size: cover;*/
            /*-o-background-size: cover;*/
            /*background-size: cover;*/
        }
        .clock-bottom {
            background-color: transparent;
            position: absolute;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 10;
            padding: 20px 0 35px 35px;
            color: #191a24;
            text-shadow: 0 1px 1px rgba(0,0,0,.4);
            text-align: left;
            font-family: Open Sans;
            font-size: larger;
        }
        .currency{
            background-color: rgba(67, 143, 165, 0.21) !important;
            border-radius: 4px;
            border: 0px solid transparent !important;
            box-shadow: 0 10px 10px rgba(0,0,0,.50)!important;
            font-family: Open Sans;
            color: #000000 !important;
            text-shadow: 0 1px 1px rgba(0,0,0,.4);
        }
        .login {
            background-color: rgba(67, 143, 165, 0.21) !important;
            border-radius: 4px;
            border: 0px solid transparent !important;
            box-shadow: 0 10px 10px rgba(0,0,0,.50)!important;
        }

        .btn {
            box-shadow: 0 10px 10px rgba(0,0,0,.25)!important;
        }

        a {
            color: #000000 !important;
            text-shadow: 0 10px 10px rgba(0,0,0,.95)!important;
        }

    </style>

    @stack('after-styles')
</head>
<body class="hold-transition">
<div class="row">
    <div class="col-sm-8 col-xs-8 hidden-sm-down" style="padding-top: 160px; font-size: 12px">

    </div>
    <div class="col-sm-4 col-xs-4" style="padding-top: 160px; padding-right: 80px; font-size: 12px">
{{--        <div class="login-box" id="app">--}}
{{--            <div class="login-logo">--}}
{{--                <img src="{{URL::asset('/img/logo/logo.png')}}" class="img-responsive center-block" alt="{{app_name()}}">--}}
{{--            </div>--}}
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">LOGIN</p>

                    {{ html()->form('POST', route('auth.login.post'))->open() }}
                    <div class="input-group mb-3">
                        {{ html()->text('username')
                                 ->class('form-control')
                                 ->placeholder(__('validation.attributes.username'))
                                 ->attribute('maxlength', 191)
                                 ->required() }}
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        {{ html()->password('password')
                                           ->class('form-control')
                                           ->placeholder(__('validation.attributes.password'))
                                           ->required() }}
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                {{ html()->label(html()->checkbox('remember', true, 1) . ' ' . __('labels.auth.remember_me'))->for('remember') }}
                            </div>
                        </div>

                        @if(config('access.captcha.login'))
                            <div class="row">
                                <div class="col">
                                    @captcha
                                    {{ html()->hidden('captcha_status', 'true') }}
                                </div><!--col-->
                            </div><!--row-->
                    @endif

                    <!-- /.col -->
                        <div class="col-4">

                        </div>
                        <!-- /.col -->
                    </div>

                    {{ form_submit(__('labels.auth.login_button'), 'btn btn-block btn-success btn-block') }}
                    {{ html()->form()->close() }}

                </div>
                <div class="card-footer p-1">
                    <p class="text-center p-0">
                        &copy; {{ date('Y') }} All rights reserved <br>
                        <b>ISA - ERP [Web Team] - Transcom Ltd</b>
                    </p>
                </div>
                <!-- /.login-card-body -->
            </div>
{{--        </div>--}}

    </div>
    <div class="clock-bottom d-none d-md-block">
        <h2 style="font-size: 3rem"><span id="time">{!! $carbon->format('h:i').'<small>'.$carbon->format('s').'</small>'.' '.$carbon->format('A') !!}</span></h2>
        <small><strong><span id="weekday">{{ $carbon->format('d') }} </span>, <span id="today">{!! $carbon->format('F').', '.$carbon->format('d').', '.$carbon->format('Y')  !!}</span></strong></small>
    </div>
</div>

<!-- /.login-box -->

{!! script(mix('js/manifest.js')) !!}
{!! script(mix('js/vendor.js')) !!}
{!! script(mix('js/app.js')) !!}

@include('includes.partials.messages')
<script type="text/javascript">
    var d = new Date();
    var weekday = new Array(7);
    weekday[0] =  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    var x = document.getElementById("weekday");
    x.innerHTML = weekday[d.getDay()];

    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var x = document.getElementById("today");
    x.innerHTML = month[d.getMonth()] + ", " + d.getDate() + ", "+d.getFullYear();

    function addZero(i) {
        if (i < 10) i = "0" + i;
        return i;
    }

    var x = document.getElementById("time");
    setInterval(function(){
        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();
        var s = d.getSeconds();
        var ampm = h >= 12 ? 'PM' : 'AM';
        h = h % 12;
        h = h ? (h < 10 ? '0'+h : h) : 12;
        m = m < 10 ? '0'+m : m;
        s = s < 10 ? '0'+s : s;
//            x.innerHTML = h + ":" + m + ":" + s + " "+ampm;
        x.innerHTML = h + ":" + m + "<small>"+ s +"</small> " +" "+ ampm +" ";
    }, 1000);

</script>
</body>
</html>
