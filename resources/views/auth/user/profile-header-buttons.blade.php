<div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
    <div class="btn-group btn-group-sm">
        @if(request()->routeIs('*.profile'))
            <a href="{{ route('user.profile.edit', $module) }}"
               class="btn bg-primary btn-flat btn-outline-primary bg-gradient-primary mr-1"><i
                        class="fas fa-pen"></i>&nbsp;Update My Profile</a>
        @endif

    </div>
</div>
