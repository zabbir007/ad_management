@extends('layouts.app')

@section('title', $page_heading)

@push('after-styles')
    <style type="text/css">
        table td, table tr {
            white-space: nowrap;
        }

        div.dataTables_filter {
            text-align: left !important;
        }
    </style>
@endpush

@section('content')
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="table-responsive">
                @component('components.table')
                    @slot('thead_content')
                        <tr>
                            <th>@lang('labels.access.users.table.last_name')</th>
                            <th>@lang('labels.access.users.table.first_name')</th>
                            <th>@lang('labels.access.users.table.username')</th>
                            <th>@lang('labels.access.users.table.email')</th>
                            <th>@lang('labels.access.users.table.roles')</th>
                            <th>@lang('labels.access.users.table.last_updated')</th>
                            <th>@lang('labels.access.users.table.status')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                    @endslot
                @endcomponent

            </div>
        @endslot
    @endcomponent

@endsection

@push('after-scripts')
    <script>
        $(function () {
            $('#datatable').dataTable($.extend(dataTableOptions, {
                processing: true,
                serverSide: true,
                order: [[0, "desc"]],
                ajax: {
                    url: '{{ route($module_route.'.get') }}',
                    type: 'post',
                    data: {status: true, trashed: false},
                    error: function (xhr, err) {
                        console.log(err);
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'first_name', searchable: true, sortable: true},
                    {data: 'last_name', searchable: true, sortable: true},
                    {data: 'username', searchable: true, sortable: true},
                    {data: 'email', searchable: true, sortable: true},
                    {data: 'roles_label', searchable: true, sortable: true},
                    {data: 'updated_at', searchable: true, sortable: true},
                    {data: 'status_label', searchable: true, sortable: true, className: 'text-center'},
                    {data: 'actions', searchable: false, sortable: false, className: 'text-left'}
                ],
            }));
        });
    </script>
@endpush
