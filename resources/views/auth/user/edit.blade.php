@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->modelForm($module, 'PATCH', route($module_route.'.update', $module->id))->class('form-horizontal')->open() }}
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label(__('validation.attributes.access.users.first_name'))->class('col-md-2 col-form-label col-form-label-sm')->for('first_name') }}

                        <div class="col-md-10">
                            {{ html()->text('first_name')
                                ->class('form-control form-control-sm')
                                ->placeholder(__('validation.attributes.access.users.first_name'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label(__('validation.attributes.access.users.last_name'))->class('col-md-2 col-form-label col-form-label-sm')->for('last_name') }}

                        <div class="col-md-10">
                            {{ html()->text('last_name')
                                ->class('form-control form-control-sm')
                                ->placeholder(__('validation.attributes.access.users.last_name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label(__('validation.attributes.access.users.username'))->class('col-md-2 col-form-label col-form-label-sm')->for('username') }}

                        <div class="col-md-10">
                            {{ html()->text('username')
                        ->class('form-control form-control-sm')
                        ->placeholder(__('validation.attributes.access.users.username'))
                        ->attribute('maxlength', 191)
                        ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label(__('validation.attributes.access.users.email'))->class('col-md-4 col-form-label col-form-label-sm')->for('email') }}

                        <div class="col-md-8">
                            {{ html()->email('email')
                        ->class('form-control form-control-sm')
                        ->placeholder(__('validation.attributes.access.users.email'))
                        ->attribute('maxlength', 191)
                        ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="form-group row">
                {{ html()->label('Abilities')->class('col-md-1 form-control-label') }}

                <div class="table-responsive col-md-10">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('labels.access.users.table.roles')</th>
                            <th>@lang('labels.access.users.table.permissions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                @if($roles->count())
                                    @foreach($roles as $role)
                                        <div class="card">
                                            <div class="card-header p-1">
                                                <div class="checkbox d-flex align-items-center">
                                                    {{ html()->label(
                                                            html()->radio('roles[]', in_array($role->name, $userRoles), $role->name)
                                                                    ->class('switch-input')
                                                                    ->id('role-'.$role->id)
                                                            . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                                                        ->class('switch switch-label switch-pill switch-primary mr-2')
                                                        ->for('role-'.$role->id) }}
                                                    {{ html()->label(ucwords($role->name))->for('role-'.$role->id) }}
                                                </div>
                                            </div>
                                            <div class="card-body p-1">
                                                @if($role->id != 1)
                                                    @if($role->permissions->count())
                                                        @foreach($role->permissions as $permission)
                                                            <i class="fas fa-dot-circle"></i> {{ ucwords($permission->name) }}
                                                        @endforeach
                                                    @else
                                                        @lang('labels.general.none')
                                                    @endif
                                                @else
                                                    @lang('labels.access.users.all_permissions')
                                                @endif
                                            </div>
                                        </div><!--card-->
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                @if($permissions->count())
                                    @foreach($permissions as $permission)
                                        <div class="checkbox d-flex align-items-center">
                                            {{ html()->label(
                                                    html()->checkbox('permissions[]', in_array($permission->name, $userPermissions), $permission->name)
                                                            ->class('switch-input')
                                                            ->id('permission-'.$permission->id)
                                                        . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                                                    ->class('switch switch-label switch-pill switch-primary mr-2')
                                                ->for('permission-'.$permission->id) }}
                                            {{ html()->label(ucwords($permission->name))->for('permission-'.$permission->id) }}
                                        </div>
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div><!--col-->
            </div><!--form-group-->
        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3">
                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}

                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Save Changes', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div><!--row-->
        @endslot
    @endcomponent
    {{ html()->closeModelForm() }}
@endsection
