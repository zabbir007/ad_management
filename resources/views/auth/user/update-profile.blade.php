@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->modelForm($module, 'PATCH', route('user.profile.update', $module->id))->class('form-horizontal')->open() }}
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label(__('validation.attributes.access.users.first_name'))->class('col-md-2 col-form-label col-form-label-sm')->for('first_name') }}

                        <div class="col-md-10">
                            {{ html()->text('first_name')
                                ->class('form-control form-control-sm')
                                ->placeholder(__('validation.attributes.access.users.first_name'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label(__('validation.attributes.access.users.last_name'))->class('col-md-2 col-form-label col-form-label-sm')->for('last_name') }}

                        <div class="col-md-10">
                            {{ html()->text('last_name')
                                ->class('form-control form-control-sm')
                                ->placeholder(__('validation.attributes.access.users.last_name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label(__('validation.attributes.access.users.username'))->class('col-md-2 col-form-label col-form-label-sm')->for('username') }}

                        <div class="col-md-10">
                            {{ html()->text('username')
                        ->class('form-control form-control-sm')
                        ->placeholder(__('validation.attributes.access.users.username'))
                        ->attribute('maxlength', 191)
                        ->required()
                         ->readonly()
                         ->disabled()}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label(__('validation.attributes.access.users.email'))->class('col-md-4 col-form-label col-form-label-sm')->for('email') }}

                        <div class="col-md-8">
                            {{ html()->email('email')
                        ->class('form-control form-control-sm')
                        ->placeholder(__('validation.attributes.access.users.email'))
                        ->attribute('maxlength', 191)
                        ->required()
                          ->readonly()
                         ->disabled()}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3">
                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}

                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Save Changes', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div><!--row-->
        @endslot
    @endcomponent
    {{ html()->closeModelForm() }}
@endsection
