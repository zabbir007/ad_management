@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tr>
                            <th>@lang('labels.access.users.tabs.content.overview.avatar')</th>
                            <td><img src="{{ $module->picture }}" class="user-profile-image"/></td>
                        </tr>

                        <tr>
                            <th>@lang('labels.access.users.tabs.content.overview.name')</th>
                            <td>{{ $module->name }}</td>
                        </tr>

                        <tr>
                            <th>@lang('labels.access.users.tabs.content.overview.email')</th>
                            <td>{{ $module->email }}</td>
                        </tr>

                        {{--            <tr>--}}
                        {{--                <th>@lang('labels.access.users.tabs.content.overview.status')</th>--}}
                        {{--                <td>@include('auth.user.includes.status', ['user' => $module])</td>--}}
                        {{--            </tr>--}}

                        {{--            <tr>--}}
                        {{--                <th>@lang('labels.access.users.tabs.content.overview.confirmed')</th>--}}
                        {{--                <td>@include('auth.user.includes.confirm', ['user' => $module])</td>--}}
                        {{--            </tr>--}}

                        <tr>
                            <th>@lang('labels.access.users.tabs.content.overview.timezone')</th>
                            <td>{{ $module->timezone }}</td>
                        </tr>

                        <tr>
                            <th>@lang('labels.access.users.tabs.content.overview.last_login_at')</th>
                            <td>
                                @if($module->last_login_at)
                                    {{ timezone()->convertToLocal($module->last_login_at) }}
                                @else
                                    N/A
                                @endif
                            </td>
                        </tr>

                        <tr>
                            <th>@lang('labels.access.users.tabs.content.overview.last_login_ip')</th>
                            <td>{{ $module->last_login_ip ?? 'N/A' }}</td>
                        </tr>
                    </table>
                </div>
            </div><!--table-responsive-->
        @endslot

        @slot('footer')
            <div class="row">
                <div class="col">
                    <small class="float-right text-muted">
                        <strong>@lang('labels.access.users.tabs.content.overview.created_at')
                            :</strong> {{ timezone()->convertToLocal($module->created_at) }}
                        ({{ $module->created_at->diffForHumans() }}),
                        <strong>@lang('labels.access.users.tabs.content.overview.last_updated')
                            :</strong> {{ timezone()->convertToLocal($module->updated_at) }}
                        ({{ $module->updated_at->diffForHumans() }})
                        @if($module->trashed())
                            <strong>@lang('labels.access.users.tabs.content.overview.deleted_at')
                                :</strong> {{ timezone()->convertToLocal($module->deleted_at) }}
                            ({{ $module->deleted_at->diffForHumans() }})
                        @endif
                    </small>
                </div><!--col-->
            </div><!--row-->
        @endslot
    @endcomponent
@endsection
