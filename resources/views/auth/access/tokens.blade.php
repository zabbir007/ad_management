@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    <passport-personal-access-tokens></passport-personal-access-tokens>
@endsection
