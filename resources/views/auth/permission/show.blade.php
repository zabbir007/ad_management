@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            {{ html()->modelForm($model, 'PATCH', route($module_route.'.update', $model->id))->class('form-horizontal')->open() }}
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-1 row">
                                {{ html()->label('Code:')->class('col-md-3')->for('code') }}
                                <div class="col-md-9">
                                    {{ html()->text('code')
                                        ->class('form-control form-control-sm')
                                        ->placeholder('Optional code')
                                        ->attribute('maxlength', 10)
                                        ->disabled()
                                    }}
                                </div><!--col-->
                            </div><!--form-group-->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-1 row">
                                {{ html()->label('Name:')->class('col-md-3')->for('name') }}

                                <div class="col-md-9">
                                    {{ html()->text('name')
                                        ->class('form-control form-control-sm')
                                        ->placeholder('Company Name')
                                        ->attribute('maxlength', 70)
                                        ->disabled()}}
                                </div><!--col-->
                            </div><!--form-group-->
                        </div>
                    </div>

                </div><!--col-->
            </div><!--row-->

            <div class="row">
                <div class="col">
                    <small class="float-right text-muted">
                        <strong>@lang('labels.access.users.tabs.content.overview.created_at')
                            :</strong> {{ timezone()->convertToLocal($model->created_at) }}
                        ({{ $model->created_at->diffForHumans() }}),
                        <strong>@lang('labels.access.users.tabs.content.overview.last_updated')
                            :</strong> {{ timezone()->convertToLocal($model->updated_at) }}
                        ({{ $model->updated_at->diffForHumans() }})
                    </small>
                </div><!--col-->
            </div><!--row-->

            {{ html()->closeModelForm() }}
        @endslot
    @endcomponent
@endsection
