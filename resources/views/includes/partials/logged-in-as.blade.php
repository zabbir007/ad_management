@impersonating
    <div class="alert alert-warning logged-in-as">
       <i class="fas fa-exclamation-triangle"></i> You are currently logged in as {{ auth()->user()->name }}. <a href="{{ route('impersonate.leave') }}">Switch back to your account</a>.
    </div><!--alert alert-warning logged-in-as-->
@endImpersonating
