@if($errors->any())
    <script>
        $(function () {
            toastr.error('{!! implode('<br/>', $errors->all()) !!}')
        });
    </script>

@elseif(session()->get('flash_success'))
    <script>
        @if(is_array(json_decode(session()->get('flash_success'), true)))
        $(function () {
            toastr.success('{{ implode('', session()->get('flash_success')->all(':message<br/>')) }}');
        });
        @else
        $(function () {
            toastr.success('{{ session()->get('flash_success') }}');
        });
        @endif
    </script>

@elseif(session()->get('flash_warning'))
    <script>
        @if(is_array(json_decode(session()->get('flash_warning'), true)))
        $(function () {
            toastr.warning('{{ implode('', session()->get('flash_warning')->all(':message<br/>')) }}');
        });
        @else
        $(function () {
            toastr.warning('{{ session()->get('flash_warning') }}');
        });
        @endif
    </script>

@elseif(session()->get('flash_info'))
    <script>
        @if(is_array(json_decode(session()->get('flash_info'), true)))
        $(function () {
            toastr.info('{{ implode('', session()->get('flash_info')->all(':message<br/>')) }}');
        });
        @else
        $(function () {
            toastr.info('{{ session()->get('flash_info') }}');
        });
        @endif
    </script>

@elseif(session()->get('flash_danger'))
    <script>
        @if(is_array(json_decode(session()->get('flash_danger'), true)))
        $(function () {
            toastr.error('{{ implode('', session()->get('flash_danger')->all(':message<br/>')) }}')
        });
        @else
        $(function () {
            toastr.error('{{ session()->get('flash_danger') }}');
        });
        @endif
    </script>

@elseif(session()->get('flash_message'))
    <script>
        @if(is_array(json_decode(session()->get('flash_message'), true)))
        $(function () {
            toastr.info('{{ implode('', session()->get('flash_message')->all(':message<br/>')) }}');
        });
        @else
        $(function () {
            toastr.info('{{ session()->get('flash_message') }}');
        });
        @endif
    </script>
@endif
