<footer class="main-footer text-sm">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Version <span>{{ config('application.version_name') }}
    </div>
    <!-- Default to the left -->
    <strong>@lang('labels.general.copyright') &copy; {{ date('Y') }} {{ app_name() }}.</strong> @lang('strings.general.all_rights_reserved')
</footer>

