<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('dashboard') }}" class="brand-link">
        <img src="{{URL::asset('/img/logo/logo.png')}}" class="img-responsive img-fluid center-block"
             alt="{{app_name()}}"
             class="brand-image img-responsive elevation-3"
             style="opacity: .8">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-1 pb-1 mb-1 d-flex">
            <div class="info">
                <a href="#" class="d-block">{{ $logged_in_user->full_name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2 text-sm">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}"
                       class="nav-link {{ active_class(request()->routeIs('dashboard')) }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>{{ __('Dashboard') }}</p>
                    </a>
                </li>
                @canany(['setup-user-index', 'setup-log-viewer-dashboard','setup-log-viewer-logs','setup-role-index','setup-permission-index','setup-api-clients','setup-api-tokens'])
                    {{--                    <li class="nav-header p-1 pt-3"><i class="fa fa-fw fa-terminal"></i>&nbsp;@lang('menus.sidebar.system')</li>--}}
                    <li class="nav-header p-1 pt-2">{{ __('Preferences') }}</li>
                    @canany(['setup-user-index', 'setup-role-index','setup-permission-index'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs(['user.*', 'role.*']), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('user.*')) }}">
                                <i class="nav-icon fas fa-users-cog"></i>
                                <p>
                                    @lang('menus.access.title')
                                    @if ($pending_approval > 0)
                                        <span class="right badge badge-danger">{{ $pending_approval }}</span>
                                    @endif
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-user-index')
                                    <li class="nav-item">
                                        <a href="{{ route('user.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('user.*')) }}">
                                            <i class="fas fa-users nav-icon"></i>
                                            <p>
                                                {{ __('User Manager') }}
                                                @if ($pending_approval > 0)
                                                    <span class="right badge badge-danger">{{ $pending_approval }}</span>
                                                @endif
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-role-index')
                                    <li class="nav-item ">
                                        <a href="{{ route('role.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('role.index')) }}">
                                            <i class="fas fa-user-lock nav-icon"></i>
                                            <p>{{ __('Role Manager') }}</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('setup-permission-index')
                                    <li class="nav-item ">
                                        <a href="{{ route('permission.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('permission.*')) }}">
                                            <i class="fas fa-user-lock nav-icon"></i>
                                            <p>{{ __('Permission Manager') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany
                    @canany(['setup-log-viewer-dashboard','setup-log-viewer-logs'])
                        {{--                    <li class="nav-header p-1 pt-3"><i class="fa fa-fw fa-terminal"></i>&nbsp;@lang('menus.sidebar.system')</li>--}}
                        <li class="nav-header p-1 pt-2">{{ __('Log Manager') }}</li>
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('log-viewer::*'), ' menu-open') }}">
                            <a href="#"
                               class="nav-link {{ active_class(request()->routeIs('log-viewer::*'), 'active') }}">
                                <i class="nav-icon fas fa-user-secret"></i>
                                <p>
                                    {{ __('System Logs') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-log-viewer-dashboard')
                                    <li class="nav-item">
                                        <a href="{{ route('log-viewer::dashboard') }}"
                                           class="nav-link {{ active_class(request()->routeIs('log-viewer::dashboard')) }}">
                                            <i class="far fa-list-alt nav-icon"></i>
                                            <p> {{ __('Log Dashboard') }}</p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-log-viewer-logs')
                                    <li class="nav-item ">
                                        <a href="{{ route('log-viewer::logs.list') }}"
                                           class="nav-link {{ active_class(request()->routeIs('log-viewer::logs.list')) }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p> {{ __('All Logs') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany
                @endcanany


                @canany(['setup-company-index', 'setup-company-create','setup-segment-index',
                    'setup-segment-create','setup-contact-index','setup-contact-create',
                    'setup-adsize-index','setup-adsize-create',
                    'setup-sitesection-index','setup-sitesection-create',
                    'setup-product-index','setup-product-create',
                    'setup-inventorytype-index','setup-inventorytype-create',
                    'setup-geotarget-index','setup-geotarget-create'])
                    <li class="nav-header p-1 pt-2">{{ __('Master Setup') }}</li>
                    @canany(['setup-segment-index', 'setup-segment-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.segment.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.segment.*')) }}">
                                <i class="nav-icon fas fa-industry"></i>
                                <p>
                                    {{ __('Segment') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-segment-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.segment.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.segment.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Segment') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-segment-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.segment.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.segment.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Segment') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany
                    @canany(['setup-contact-index', 'setup-contact-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.contact.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.contact.*')) }}">
                                <i class="nav-icon fas fa-address-book"></i>
                                <p>
                                    {{ __('Contact') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-contact-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.contact.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.contact.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Contact') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-contact-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.contact.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.contact.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Contact') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany
                    @canany(['setup-adsize-index', 'setup-adsize-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.adsize.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.adsize.*')) }}">
                                <i class="nav-icon fas fa-ad"></i>
                                <p>
                                    {{ __('Ad Size') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-adsize-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.adsize.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.adsize.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Size') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan
                                @can('setup-adsize-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.adsize.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.adsize.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Size') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany
                    @canany(['setup-sitesection-index', 'setup-sitesection-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.sitesection.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.sitesection.*')) }}">
                                <i class="nav-icon fas fa-sitemap"></i>
                                <p>
                                    {{ __('Site Section') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-sitesection-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.sitesection.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.sitesection.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Section') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan
                                @can('setup-sitesection-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.sitesection.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.sitesection.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Section') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany
                    @canany(['setup-product-index', 'setup-product-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.product.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.product.*')) }}">
                                <i class="nav-icon fas fa-dolly-flatbed"></i>
                                <p>
                                    {{ __('Products') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-product-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.product.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.product.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Product') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan
                                @can('setup-product-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.product.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.product.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Product') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany
                    @canany(['setup-inventorytype-index', 'setup-inventorytype-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.inventorytype.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.inventorytype.*')) }}">
                                <i class="nav-icon fas fa-warehouse"></i>
                                <p>
                                    {{ __('Inventory Type') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-inventorytype-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.inventorytype.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.inventorytype.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Inventory') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan
                                @can('setup-inventorytype-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.inventorytype.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.inventorytype.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Inventory') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany

                    @canany(['setup-geotarget-index', 'setup-geotarget-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.geotarget.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.geotarget.*')) }}">
                                <i class="nav-icon fas fa-map-marker-alt"></i>
                                <p>
                                    {{ __('Geo Target') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-geotarget-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.geotarget.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.geotarget.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Geo Target') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan
                                @can('setup-geotarget-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.geotarget.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.geotarget.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Geo') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany
                @endcanany
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
