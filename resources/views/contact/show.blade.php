@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            {{ html()->modelForm($module, 'PATCH', route($module_route.'.update', $module->id))->class('form-horizontal')->open() }}
            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Designation')->class('col-md-3 col-form-label col-form-label-sm')->for('name') }}

                        <div class="col-md-8">
                            {{ html()->text('designation')
                                ->class('form-control form-control-sm')
                                ->placeholder('Designation')
                                ->attribute('maxlength', 20)
                                ->disabled() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Name')->class('col-md-3 col-form-label col-form-label-sm')->for('name') }}

                        <div class="col-md-7">
                            {{ html()->text('name')
                                ->class('form-control form-control-sm')
                                ->placeholder('Name')
                                ->attribute('maxlength', 20)
                                ->disabled() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Email')->class('col-md-3 col-form-label col-form-label-sm')->for('email') }}

                        <div class="col-md-8">
                            {{ html()->email('email')
                                ->class('form-control form-control-sm')
                                ->placeholder('Email')
                                ->attribute('maxlength', 191)
                                ->disabled()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Contact Number')->class('col-md-3 col-form-label col-form-label-sm')->for('contact_no') }}

                        <div class="col-md-7">
                            {{ html()->text('contact_no')
                                ->class('form-control form-control-sm')
                                ->placeholder('Contact Number')
                                ->attribute('maxlength', 20)
                                ->disabled() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <small class="float-right text-muted">
                        <strong>@lang('labels.access.users.tabs.content.overview.created_at')
                            :</strong> {{ timezone()->convertToLocal($module->created_at) }}
                        ({{ $module->created_at->diffForHumans() }}),
                        <strong>@lang('labels.access.users.tabs.content.overview.last_updated')
                            :</strong> {{ timezone()->convertToLocal($module->updated_at) }}
                        ({{ $module->updated_at->diffForHumans() }})
                    </small>
                </div><!--col-->
            </div><!--row-->

            {{ html()->closeModelForm() }}
        @endslot
    @endcomponent
@endsection
